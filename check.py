org_data = 'RawData'
check_data = 'Jira_tickets/BFC-3/dst'

import os, glob, shutil
from tqdm import tqdm

org_list = [i for i in glob.glob(org_data+os.sep+'**',recursive=True) if i.endswith('.png')]
orig_dict = {i.split(os.sep)[-1]:i for i in org_list}
check_list = [i for i in glob.glob(check_data+os.sep+'**',recursive=True) if i.endswith('.png')]
check_dict = {i.split('$')[-1]:True for i in check_list}

print('orignal data has {0} images'.format(len(org_list)))
print('check data has {0} images'.format(len(check_list)))

missing_list = []
for k, v in orig_dict.items():
	if not k in check_dict:
		missing_list.append(v)

# Check original data if there is duplicated path
path_list = list(set(orig_dict.keys()))
print('original num', len(path_list))

print('missing num', len(missing_list))