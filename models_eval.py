import math
import torch
import torchvision
from torch import nn
from torch.nn import functional as F
import torchvision
from torch.utils.data import DataLoader, Dataset
import albumentations as alb
import albumentations
from albumentations import Compose, RandomCrop, Normalize, HorizontalFlip, Resize 
from albumentations.pytorch import ToTensor
from skimage import io
from torch import optim
import glob 
import numpy as np
import os
from scheduler import CycleScheduler

class CustomModel(nn.Module):
    def __init__(self, args):
        super().__init__()

        kwargs = {}
        backbone = args['backbone']
#         if args.backbone.startswith('mem-'):
#             kwargs['memory_efficient'] = True
#             backbone = args.backbone[4:]
        
        if backbone.startswith('densenet'):
           
             
            pretrained_backbone = getattr(torchvision.models, backbone)(pretrained=True, **kwargs)
            self.features = pretrained_backbone.features
#             self.features.conv0 = first_conv
            features_num = pretrained_backbone.classifier.in_features
        elif backbone.startswith('mobilenet_v2'):
            pretrained_backbone = torch.hub.load('pytorch/vision:v0.6.0', 'mobilenet_v2', pretrained=True)
            self.features = pretrained_backbone.features
            features_num = pretrained_backbone.classifier[1].in_features
            
        elif backbone.startswith('resnet') or backbone.startswith('resnext'):
            first_conv = nn.Conv2d(6, 64, 7, 2, 3, bias=False)
            pretrained_backbone = getattr(torchvision.models, backbone)(pretrained=True, **kwargs)
            first_conv = pretrained_backbone.conv1
            self.features = nn.Sequential(
                first_conv,
                pretrained_backbone.bn1,
                pretrained_backbone.relu,
                pretrained_backbone.maxpool,
                pretrained_backbone.layer1,
                pretrained_backbone.layer2,
                pretrained_backbone.layer3,
                pretrained_backbone.layer4,
            )
            features_num = pretrained_backbone.fc.in_features
#         elif backbone.startswith('xresnet'):
#             print('loading %s' % (backbone))
#             pretrained_backbone = globals()[backbone](pretrained=False, **kwargs).cuda()
# #            
#             self.features = pretrained_backbone
# #            
#             features_num = pretrained_backbone.fc.in_features
# #             
        elif backbone.startswith('efficientnet'):
            from efficientnet_pytorch import EfficientNet
            self.efficientnet = EfficientNet.from_pretrained(backbone)
#             first_conv = nn.Conv2d(6, self.efficientnet._conv_stem.out_channels, kernel_size=3, stride=2, padding=1, bias=False)
#             self.efficientnet._conv_stem = first_conv
            self.features = self.efficientnet.extract_features
            features_num = self.efficientnet._conv_head.out_channels
        else:
            raise ValueError('wrong backbone')

#         self.concat_cell_type = args.concat_cell_type
        self.classes = args['classes']

#         features_num = features_num + (4 if self.concat_cell_type else 0)

        self.neck = nn.Sequential(
            nn.BatchNorm1d(features_num),
            nn.Linear(features_num, args['embedding_size'], bias=False),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(args['embedding_size']),
#             nn.Linear(args['embedding_size'], args['embedding_size'], bias=False),
#             nn.BatchNorm1d(args['embedding_size']),
        )
        self.arc_margin_product = ArcMarginProduct(args['embedding_size'], args['classes'])

#         if args.head_hidden is None:
        self.head = nn.Linear(args['embedding_size'], args['classes'])
#         else:
#             self.head = []
#             for input_size, output_size in zip([args.embedding_size] + args.head_hidden, args.head_hidden):
#                 self.head.extend([
#                     nn.Linear(input_size, output_size, bias=False),
#                     nn.BatchNorm1d(output_size),
#                     nn.ReLU(),
#                 ])
#             self.head.append(nn.Linear(args.head_hidden[-1], arg['classes']))
#             self.head = nn.Sequential(*self.head)

        for m in self.modules():
            if isinstance(m, nn.BatchNorm1d) or isinstance(m, nn.BatchNorm2d):
                m.momentum = args['bn_mom']
        self.eval_on_arc_margin = args['eval_on_arc_margin']
    
    def forward(self, x, eval=True):
#         print(x)
        x = self.embed(x)
        x_softmax = self.classify(x)
        x_arc = self.metric_classify(x)
        if eval:
            if self.eval_on_arc_margin:
                return x_arc
            return x_softmax
    
            
        return x_arc, x_softmax
#         return x_softmax
    
    def embed(self, x):
        x = self.features(x)

        x = F.adaptive_avg_pool2d(x, (1, 1))
        x = x.view(x.size(0), -1)
#         if self.concat_cell_type:
#             x = torch.cat([x, s], dim=1)

        embedding = self.neck(x)
        return embedding

    def metric_classify(self, embedding):
        return self.arc_margin_product(embedding)

    def classify(self, embedding):
        return self.head(embedding)
    


class LossFunction(nn.Module):
    def __init__(self, args):
        super().__init__()

        self.args = args
#         self.model = model
        self.metric_crit = ArcFaceLoss(args)
        self.crit = DenseCrossEntropy(args)
    
    def forward(self, y_p, y_t, eval=False):        
        if eval:
            if self.args['eval_on_arc_margin']:
                metric_loss = self.metric_crit(y_p, y_t)
                return metric_loss
            loss = self.crit(y_p, y_t)
            return loss
        y_p_arc, y_p_softmax = y_p
        loss = self.crit(y_p_softmax, y_t)
        metric_loss = self.metric_crit(y_p_arc, y_t)
                
        coeff = self.args['metric_loss_coeff']
        return loss * (1 - coeff) + metric_loss * coeff#, acc


class FocalLoss(nn.Module):
    def __init__(self, alpha=1, gamma=2, logits=False, reduction='elementwise_mean'):
        super(FocalLoss, self).__init__()
        self.alpha = alpha
        self.gamma = gamma
        self.logits = logits
        self.reduction = reduction

    def forward(self, inputs, targets):
        if self.logits:
            BCE_loss = F.cross_entropy(inputs, targets, reduction='none')
        else:
            BCE_loss = F.cross_entropy(inputs, targets, reduction='none')
        pt = torch.exp(-BCE_loss)
        F_loss = self.alpha * (1-pt)**self.gamma * BCE_loss

        if self.reduction is None:
            return F_loss
        else:
            return torch.mean(F_loss)

class DenseCrossEntropy(nn.Module):
    def __init__(self, args):
        super().__init__()
        class_weights = args['class_weights']
        weights = [class_weights[class_] if class_weights.get(class_) else 1 for i, class_ in enumerate(args['class_names'])]
        self.class_weights = torch.tensor(weights).float().to('cuda')
        
    def forward(self, x, target):
        x = x.float()
        target = target.float()
        logprobs = torch.nn.functional.log_softmax(x, dim=-1)
        loss = -logprobs * target
        
        loss = loss * self.class_weights
        loss = loss.sum(-1)
        return loss.mean()


class ArcFaceLoss(nn.modules.Module):
    def __init__(self, args):#s=30.0, m=0.5):
        super().__init__()
        s = args['s']
        m = args['m']
        self.crit = DenseCrossEntropy(args)
        self.s = s
        self.cos_m = math.cos(m)
        self.sin_m = math.sin(m)
        self.th = math.cos(math.pi - m)
        self.mm = math.sin(math.pi - m) * m

    def forward(self, logits, labels):
        logits = logits.float()
        labels = labels.float()
        cosine = logits
        sine = torch.sqrt(1.0 - torch.pow(cosine, 2))
        phi = cosine * self.cos_m - sine * self.sin_m
        phi = torch.where(cosine > self.th, phi, cosine - self.mm)

        output = (labels * phi) + ((1.0 - labels) * cosine)
        output *= self.s
        loss = self.crit(output, labels)
        return loss #/ 2


class ArcMarginProduct(nn.Module):
    def __init__(self, in_features, out_features):
        super().__init__()
        self.weight = nn.Parameter(torch.FloatTensor(out_features, in_features))
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)

    def forward(self, features):
        cosine = F.linear(F.normalize(features), F.normalize(self.weight))
        return cosine


    
    
import torch.nn as nn
import torch
import math
import torch.utils.model_zoo as model_zoo
from fastai.torch_core import Module


__all__ = ['XResNet', 'xresnet18', 'xresnet34_2', 'xresnet50_2', 'xresnet101', 'xresnet152']


def conv3x3(in_planes, out_planes, stride=1):
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride, padding=1, bias=False)


class BasicBlock(Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(BasicBlock, self).__init__()
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = nn.BatchNorm2d(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = nn.BatchNorm2d(planes)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None: residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out


class Bottleneck(Module):
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(inplanes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride,
                               padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, planes * self.expansion, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(planes * self.expansion)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None: residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out

def conv2d(ni, nf, stride):
    return nn.Sequential(nn.Conv2d(ni, nf, kernel_size=3, stride=stride, padding=1, bias=False),
                         nn.BatchNorm2d(nf), nn.ReLU(inplace=True))

class XResNet(Module):

    def __init__(self, block, layers, c_out=1000):
        self.inplanes = 64
        super(XResNet, self).__init__()
        self.conv1 = conv2d(3, 32, 2)
        self.conv2 = conv2d(32, 32, 1)
        self.conv3 = conv2d(32, 64, 1)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2)
        self.avgpool = nn.AdaptiveAvgPool2d(1)
        self.fc = nn.Linear(512 * block.expansion, c_out)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

        for m in self.modules():
            if isinstance(m, BasicBlock): m.bn2.weight = nn.Parameter(torch.zeros_like(m.bn2.weight))
            if isinstance(m, Bottleneck): m.bn3.weight = nn.Parameter(torch.zeros_like(m.bn3.weight))
            if isinstance(m, nn.Linear): m.weight.data.normal_(0, 0.01)

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            layers = []
            if stride==2: layers.append(nn.AvgPool2d(kernel_size=2, stride=2))
            layers += [
                nn.Conv2d(self.inplanes, planes * block.expansion, kernel_size=1, stride=1, bias=False),
                nn.BatchNorm2d(planes * block.expansion) ]
            downsample = nn.Sequential(*layers)

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks): layers.append(block(self.inplanes, planes))
        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)

        return x
    def extract_features(self, *x):
        return self.forward(x[0])


def xresnet18(pretrained=False, **kwargs):
    """Constructs a XResNet-18 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = XResNet(BasicBlock, [2, 2, 2, 2], **kwargs)
    if pretrained: model.load_state_dict(model_zoo.load_url(model_urls['xresnet18']))
    return model


def xresnet34_2(pretrained=False, **kwargs):
    """Constructs a XResNet-34 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = XResNet(BasicBlock, [3, 4, 6, 3], **kwargs)
    if pretrained: model.load_state_dict(model_zoo.load_url(model_urls['xresnet34']))
    return model


def xresnet50_2(pretrained=False, **kwargs):
    """Constructs a XResNet-50 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = XResNet(Bottleneck, [3, 4, 6, 3], **kwargs)
    if pretrained: model.load_state_dict(model_zoo.load_url(model_urls['xresnet50']))
    return model


def xresnet101(pretrained=False, **kwargs):
    """Constructs a XResNet-101 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = XResNet(Bottleneck, [3, 4, 23, 3], **kwargs)
    if pretrained: model.load_state_dict(model_zoo.load_url(model_urls['xresnet101']))
    return model


def xresnet152(pretrained=False, **kwargs):
    """Constructs a XResNet-152 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = XResNet(Bottleneck, [3, 8, 36, 3], **kwargs)
    if pretrained: model.load_state_dict(model_zoo.load_url(model_urls['xresnet152']))
    return model