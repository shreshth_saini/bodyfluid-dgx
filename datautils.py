import torchvision
# torchvision.models.resnet18()
from torch.utils.data import DataLoader, Dataset
import albumentations as alb
import albumentations
from albumentations import Compose, RandomCrop, Normalize, HorizontalFlip, Resize
from albumentations.pytorch import ToTensor
from skimage import io
from torch import optim
import glob
import numpy as np
import os
from scheduler import CycleScheduler

# classes = np.array(['SQEC', 'UNCL', 'HYAL',
#                     'DRBC', 'NHC',
#                     'BACT', 'NSE', 'CRYS', 'SPRM',
#         'RBC', 'ART', 'YST', 'MUCS', 'WBC', 'WBCC'])

classes = np.array(['RBC', 'WBC_P', 'UNCL', 'BACT', 'ART', 'MUCS', 'OTHER_CELL', 'WBCC_C',
       'YST_F'])
additional_mappings = {} #{'DRBC': 'RBC', 'MAR': 'RBC', 'RBCC': 'RBC', 'SQECC': 'SQEC'}
batch_size = 256

img_size = 150

n_classes = len(classes)
num_workers = 8
mean=[0.4914, 0.4822, 0.4465]
std=[0.2023, 0.1994, 0.2010]

def rotate_flip_aug(p=1.0):
    return alb.Compose([alb.RandomRotate90(),
                            alb.Flip(),
                            alb.Transpose(),], p=p)

def non_prespective_augmentations(p=0.5):
     return alb.ShiftScaleRotate(shift_limit=0.2, scale_limit=0.2, rotate_limit=90, p=p)
         
        
def optical_augmentations(p=0.5):
    return alb.OneOf([
            alb.OpticalDistortion(distort_limit=0.2, shift_limit=0.2, p=1.0),
            alb.GridDistortion(p=1.0),
            alb.IAAPiecewiseAffine(p=1.0),], p=p)


train_tfms = Compose([alb.Resize(img_size, img_size),
                      rotate_flip_aug(p=1),
                      # strong augmentations p=0.3
                      alb.Compose([
                          alb.OneOrOther(
                              first=non_prespective_augmentations(p=1.0),
                              second=optical_augmentations(p=1.0), 
                              p=0.7),],
                          p=0.3),
                      Normalize(
                          mean=mean,
                          std=std,
                      ),
                      ToTensor(), ])

test_tfms = Compose([alb.Resize(img_size, img_size),

                     Normalize(
                         mean=mean,
                         std=std,
                     ),
                     ToTensor(), ])


class ImageDataset(Dataset):
    def __init__(self, file_paths, tfms, label_vector=None, weight=1.0,**init_kwargs):
        self.filepaths = file_paths 
        self.tfms = tfms
        self.label_vector = label_vector
        
        if self.label_vector is None:
            self.classes = np.array(list(set([os.path.split(os.path.dirname(x))[-1] for x in file_paths])))
        self.weight = weight
    def __len__(self):
        return len(self.filepaths)

    def __getitem__(self, idx):
        X = io.imread(self.filepaths[idx])[:,:,:3]
        if self.tfms:
            X = self.tfms(image=X)
            X = X['image']
        if self.label_vector is None:
            y = os.path.split(os.path.dirname(self.filepaths[idx]))[-1]
            if y not in self.classes:
                y = additional_mappings.get(y)

            y = np.where(self.classes == y, 1.0, 0.0)  # .astype(float)#.argmax()
            if y.sum() == 0:
                raise AssertionError (f"{os.path.split(os.path.dirname(self.filepaths[idx]))[-1]} Class not Found")
        else:
            y = self.label_vector[idx]
            if y not in self.classes:
                y = additional_mappings.get(y)

            y = np.where(self.classes == y, 1.0, 0.0)  # .astype(float)#.argmax()
            if y.sum() == 0:
                raise AssertionError (f"{self.label_vector[idx]} Class not Found")
 
            
           
        return X, y, self.weight

class UnLabeledImageDataset(Dataset):
    def __init__(self, file_paths, tfms, **init_kwargs):
        self.filepaths = file_paths
        self.tfms = tfms
#         self.classes = np.array(list(set([os.path.split(os.path.dirname(x))[-1] for x in file_paths])))

    def __len__(self):
        return len(self.filepaths)

    def __getitem__(self, idx):
        X = io.imread(self.filepaths[idx])[:,:,:3]
        if self.tfms:
            X = self.tfms(image=X)
            X = X['image']
        return X, self.filepaths[idx]


def get_train_dl(all_train_filepaths, batch_size=batch_size, num_workers=num_workers):
    global n_classes
    global classes
    train_dataset = ImageDataset(all_train_filepaths, tfms=train_tfms)
    if classes is not None:
        train_dataset.classes = classes
        n_classes = len(classes)
    else: 
        classes = train_dataset.classes 
        
    train_dl = DataLoader(train_dataset, shuffle=True, batch_size=batch_size)
    return train_dl

def get_train_dataset(filepaths, class_names, weight=1.0, labels=None):
    dataset = ImageDataset(filepaths, weight=weight, label_vector=labels, tfms=train_tfms)
    dataset.classes = class_names
    return dataset

def get_test_dataset(filepaths, class_names, weight=1.0, labels=None):
    dataset = ImageDataset(filepaths, weight=weight, label_vector=labels, tfms=test_tfms)
    dataset.classes = class_names
    return dataset
# def concat_filepaths_get_dataset():
    
def get_test_dl(all_test_filepaths, batch_size=batch_size, num_workers=num_workers):
    
    test_dataset = ImageDataset(all_test_filepaths, tfms=test_tfms)
    test_dataset.classes = classes
    test_dl = DataLoader(test_dataset, shuffle=False, batch_size=batch_size, num_workers=num_workers)
    return test_dl

def get_dl_from_ds(dataset, num_workers=num_workers, batch_size=batch_size):
    dl = DataLoader(dataset, shuffle=True, batch_size=batch_size, num_workers=num_workers)
    return dl
    

def get_psuedolabled_dl(filepaths, labels, batch_size=batch_size):
    dataset = ImageDataset(filepaths, label_vector=labels, tfms=train_tfms)
    dataset.classes = classes
    dl = DataLoader(dataset, shuffle=False, batch_size=batch_size)
    return dl


def create_unlabeled_loader(filepaths, batch_size=batch_size):
    dataset = UnLabeledImageDataset(filepaths, tfms=test_tfms)
    dl = DataLoader(dataset, shuffle=False, batch_size=batch_size)
    return dl