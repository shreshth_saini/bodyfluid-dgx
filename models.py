import math
import torch
import torchvision
from torch import nn
from torch.nn import functional as F
import torchvision
from torch.utils.data import DataLoader, Dataset
import albumentations as alb
import albumentations
from albumentations import Compose, RandomCrop, Normalize, HorizontalFlip, Resize 
from albumentations.pytorch import ToTensor
from skimage import io
from torch import optim
import glob 
import numpy as np
import os
from scheduler import CycleScheduler
import xresnet
try:
    from vit_pytorch import ViT
except: 
    print('Warning: visual transformers not installed, ViT will not work')
    pass
class CustomModel(nn.Module):
    def __init__(self, args):
        super().__init__()

        kwargs = {}
        backbone = args['backbone']
#         if args.backbone.startswith('mem-'):
#             kwargs['memory_efficient'] = True
#             backbone = args.backbone[4:]
        
        if backbone.startswith('densenet'):
#             channels = 96 if backbone == 'densenet161' else 64
#             first_conv = nn.Conv2d(6, channels, 7, 2, 3, bias=False)
             
            pretrained_backbone = getattr(torchvision.models, backbone)(pretrained=True, **kwargs)
            self.features = pretrained_backbone.features
#             self.features.conv0 = first_conv
            features_num = pretrained_backbone.classifier.in_features
        elif backbone.startswith('mobilenet_v2'):
            pretrained_backbone = torch.hub.load('pytorch/vision:v0.6.0', 'mobilenet_v2', pretrained=True)
            self.features = pretrained_backbone.features
            features_num = pretrained_backbone.classifier[1].in_features
            
        elif backbone.startswith('resnet') or backbone.startswith('resnext'):
            first_conv = nn.Conv2d(6, 64, 7, 2, 3, bias=False)
            pretrained_backbone = getattr(torchvision.models, backbone)(pretrained=True, **kwargs)
            first_conv = pretrained_backbone.conv1
            self.features = nn.Sequential(
                first_conv,
                pretrained_backbone.bn1,
                pretrained_backbone.relu,
                pretrained_backbone.maxpool,
                pretrained_backbone.layer1,
                pretrained_backbone.layer2,
                pretrained_backbone.layer3,
                pretrained_backbone.layer4,
            )
            features_num = pretrained_backbone.fc.in_features
        elif backbone.startswith('xresnet'):
            pretrained_backbone =  getattr(xresnet, backbone)(pretrained=True, **kwargs)
            self.features = pretrained_backbone[:-3]
            features_num = pretrained_backbone[-1].in_features
# #             
        elif backbone.startswith('efficientnet'):
            from efficientnet_pytorch import EfficientNet
            self.efficientnet = EfficientNet.from_pretrained(backbone)
#             first_conv = nn.Conv2d(6, self.efficientnet._conv_stem.out_channels, kernel_size=3, stride=2, padding=1, bias=False)
#             self.efficientnet._conv_stem = first_conv
            self.features = self.efficientnet.extract_features
            features_num = self.efficientnet._conv_head.out_channels
        else:
            raise ValueError('wrong backbone')

#         self.concat_cell_type = args.concat_cell_type
        self.classes = args['classes']

#         features_num = features_num + (4 if self.concat_cell_type else 0)

        self.neck = nn.Sequential(
            nn.BatchNorm1d(features_num),
            nn.Linear(features_num, args['embedding_size'], bias=False),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(args['embedding_size']),
            nn.Linear(args['embedding_size'], args['embedding_size'], bias=False),
            nn.BatchNorm1d(args['embedding_size']),
        )
#         self.neck = nn.Sequential(
#             nn.BatchNorm1d(features_num),
#             nn.Linear(features_num, args['embedding_size'], bias=False),
#             nn.ReLU(inplace=True),
#             nn.BatchNorm1d(args['embedding_size']),
#             nn.Linear(args['embedding_size'], args['embedding_size'], bias=False),
#             nn.BatchNorm1d(args['embedding_size']),
#         )
        self.arc_margin_product = ArcMarginProduct(args['embedding_size'], args['classes'])
        self.head = nn.Linear(args['embedding_size'], args['classes'])
#        
        for m in self.modules():
            if isinstance(m, nn.BatchNorm1d) or isinstance(m, nn.BatchNorm2d):
                m.momentum = args['bn_mom']
        
    
    def forward(self, x, return_arc_margin=True):
#         print(x)
        x = self.embed(x)
        x_softmax = self.classify(x)
        if not return_arc_margin:
            return x_softmax
        else:
            x_arc = self.arc_margin_product(x)
            return x_softmax, x_arc
        
    def embed(self, x):
        x = self.features(x)

        x = F.adaptive_avg_pool2d(x, (1, 1))
        x = x.view(x.size(0), -1)
#         if self.concat_cell_type:
#             x = torch.cat([x, s], dim=1)

        embedding = self.neck(x)
        return embedding

    def classify(self, embedding):
        return self.head(embedding)
    
        

class LossFunction(nn.Module):
    def __init__(self, args):
        super().__init__()

        self.args = args
        self.crit = DenseCrossEntropy(args)
        self.metric_crit = ArcFaceLoss(args)

    
    def forward(self, y_p, y_t,):
        y_p_classify, y_p_arc = y_p
        loss = self.crit(y_p_classify, y_t)
        metric_loss = self.metric_crit(y_p_arc, y_t)
        coeff = self.args['metric_loss_coeff']
        return loss * (1 - coeff) + metric_loss * coeff#, acc        


class DCE(nn.Module):
    def __init__(self, args):
        super().__init__()
        self.args = args
        self.crit = DenseCrossEntropy(args)
    
    def forward(self, y_p, y_t,):
        y_p_classify = y_p
        loss = self.crit(y_p_classify, y_t)
        return loss        


class FocalLoss(nn.Module):
    def __init__(self, alpha=1, gamma=2, logits=False, reduction='elementwise_mean'):
        super(FocalLoss, self).__init__()
        self.alpha = alpha
        self.gamma = gamma
        self.logits = logits
        self.reduction = reduction

    def forward(self, inputs, targets):
        if self.logits:
            BCE_loss = F.cross_entropy(inputs, targets, reduction='none')
        else:
            BCE_loss = F.cross_entropy(inputs, targets, reduction='none')
        pt = torch.exp(-BCE_loss)
        F_loss = self.alpha * (1-pt)**self.gamma * BCE_loss

        if self.reduction is None:
            return F_loss
        else:
            return torch.mean(F_loss)

class DenseCrossEntropy(nn.Module):
    def __init__(self, args):
        super().__init__()
        class_weights = args['class_weights']
        weights = [class_weights[class_] if class_weights.get(class_) else 1 for i, class_ in enumerate(args['class_names'])]
        self.class_weights = torch.tensor(weights).float().to('cuda')
        
    def forward(self, x, target):
        x = x.float()
        target = target.float()
        logprobs = torch.nn.functional.log_softmax(x, dim=-1)
        loss = -logprobs * target
        
        loss = loss * self.class_weights
        loss = loss.sum(-1)
        return loss.mean()
    
class ArcFaceLoss(nn.modules.Module):
    def __init__(self, args, crit=None):#s=30.0, m=0.5):
        super().__init__()
        s = args['s']
        m = args['m']
        if crit:
            self.crit = crit
        else:
            self.crit = DenseCrossEntropy(args)
        print('Initialising ArcFaceLoss with', self.crit)
        self.s = s
        self.cos_m = math.cos(m)
        self.sin_m = math.sin(m)
        self.th = math.cos(math.pi - m)
        self.mm = math.sin(math.pi - m) * m

    def forward(self, logits, labels):
        logits = logits.float()
        labels = labels.float()
        cosine = logits
        sine = torch.sqrt(1.0 - torch.pow(cosine, 2))
        phi = cosine * self.cos_m - sine * self.sin_m
        phi = torch.where(cosine > self.th, phi, cosine - self.mm)

        output = (labels * phi) + ((1.0 - labels) * cosine)
        output *= self.s
        loss = self.crit(output, labels)
        return loss #/ 2


class ArcMarginProduct(nn.Module):
    def __init__(self, in_features, out_features):
        super().__init__()
        self.weight = nn.Parameter(torch.FloatTensor(out_features, in_features))
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)

    def forward(self, features):
        cosine = F.linear(F.normalize(features), F.normalize(self.weight))
        return cosine

    
    
import torchvision.models as models
import torch.nn as nn
import torch
import torch.nn.functional as F
from efficientnet_pytorch import EfficientNet

# class Model(torch.nn.Module):
#     def __init__(self, features_dim=128):
#         super(Model, self).__init__()
#         self.backbone = models.resnet18(pretrained=True)
#         self.backbone.fc = torch. nn.Linear(512, 256)
#         self.backbone_dim = self.backbone.fc.out_features
#         self.head = nn.Sequential(
#                     nn.Linear(self.backbone_dim, self.backbone_dim),
#                     nn.BatchNorm1d(self.backbone_dim),
#                     nn.ReLU(), nn.Linear(self.backbone_dim, features_dim))
 
#     def forward(self, x):
#         features = self.head(self.backbone(x))
#         features = F.normalize(features, dim = -1)
#         return features

# class ResNetModel(torch.nn.Module):
#     def __init__(self, features_dim=128, v='18', mlp_dim=512):
#         super(ResNetModel, self).__init__()
#         self.backbone = getattr(models, f'resnet{v}')(pretrained=True)
#         backbone_dim = self.backbone.fc.in_features
# #         self.backbone = nn.Sequential(*list(model.children())[:-1])
#         self.backbone.fc = nn.Identity()
#         self.head = nn.Sequential(
#                     nn.Flatten(),
#                     nn.Linear(backbone_dim, mlp_dim),
#                     nn.BatchNorm1d(mlp_dim),
#                     nn.ReLU(), nn.Linear(mlp_dim, features_dim))
 
#     def forward(self, x):
#         x = self.backbone(x)
#         features = self.head(x)
#         features = F.normalize(features, dim = -1)
#         return features

    

class EfficientNetModel(torch.nn.Module):
    def __init__(self, features_dim=128, v='b0', mlp_dim=1024):
        super(EfficientNetModel, self).__init__()
        self.backbone = EfficientNet.from_pretrained(f'efficientnet-{v}')
        backbone_dim = self.backbone._fc.in_features
#         self.backbone = nn.Sequential(*list(model.children())[:-3]) # till avgpool
        
        self.backbone._dropout = nn.Identity()
        self.backbone._fc = nn.Identity()
        self.backbone._swish = nn.Identity()
        self.head = nn.Sequential(
                    nn.Flatten(),
                    nn.Linear(backbone_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, features_dim),
        )
 
    def forward(self, x):
        x = self.backbone(x)
        features = self.head(x)
        features = F.normalize(features, dim = -1)
        return features

    
class EfficientNetModelMLP(torch.nn.Module):
    def __init__(self, features_dim=2048, v='b2', mlp_dim=2048):
        super(EfficientNetModelMLP, self).__init__()
        self.backbone = EfficientNet.from_pretrained(f'efficientnet-{v}')
        backbone_dim = self.backbone._fc.in_features
#         self.backbone = torch.nn.Sequential(*(list(self.backbone.children())[:-3]))
#         self.backbone = nn.Sequential(*list(model.children())[:-3]) # till avgpool
#         backbone_dim = self.backbone[-2].num_features
        self.backbone._dropout = nn.Identity()
        self.backbone._fc = nn.Identity()
        self.backbone._swish = nn.Identity()
        self.mlp = nn.Sequential(
                    nn.Flatten(),
                    nn.Linear(backbone_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
        )
        self.head = nn.Sequential(
                    
                    nn.Linear(mlp_dim, mlp_dim//4),
                    nn.BatchNorm1d(mlp_dim//4),
                    nn.ReLU(),
                    nn.Linear(mlp_dim//4, features_dim),
        )
    def backbone_mlp(self, x):
        x = self.backbone(x)
        x = self.mlp(x)
        return x
    
    def forward(self, x):
        x = self.backbone(x)
        x = self.mlp(x)
        x = self.head(x)
        x = F.normalize(x, dim = -1)
        return x

   
class DenseNetModel(torch.nn.Module):
    def __init__(self, features_dim=128):
        super(DesNet, self).__init__()
        self.backbone = models.densenet161(pretrained=True)
#         self.backbone.fc = torch. nn.Linear(512, 256)
        self.backbone_dim = self.backbone.classifier.out_features
        self.head = nn.Sequential(
                    nn.Linear(self.backbone_dim, self.backbone_dim),
                    nn.BatchNorm1d(self.backbone_dim),
                    nn.ReLU(), nn.Linear(self.backbone_dim, features_dim))
 
    def forward(self, x):
        features = self.head(self.backbone(x))
        features = F.normalize(features, dim = -1)
        return features
    

class ResNet50Model(torch.nn.Module):
    def __init__(self, features_dim=256):
        super(ResNet50Model, self).__init__()
        self.backbone = models.resnet50(pretrained=True)
#         self.backbone.fc = torch. nn.Linear(1000, 1000)
        self.backbone_dim = self.backbone.fc.out_features
        self.head = nn.Sequential(
                    nn.Linear(self.backbone_dim, self.backbone_dim),
                    nn.BatchNorm1d(self.backbone_dim),
                    nn.ReLU(), nn.Linear(self.backbone_dim, features_dim))
 
    def forward(self, x):
        features = self.head(self.backbone(x))
        features = F.normalize(features, dim = -1)
        return features

class ResNetModel(torch.nn.Module):
    def __init__(self, features_dim=128, v='18', mlp_dim=512):
        super(ResNetModel, self).__init__()
        self.backbone = getattr(models, f'resnet{v}')(pretrained=True)
        backbone_dim = self.backbone.fc.in_features
#         self.backbone = nn.Sequential(*list(model.children())[:-1])
        self.backbone.fc = nn.Identity()
        self.head = nn.Sequential(
                    nn.Flatten(),
                    nn.Linear(backbone_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(), nn.Linear(mlp_dim, features_dim))
 
    def forward(self, x):
        x = self.backbone(x)
        features = self.head(x)
        features = F.normalize(features, dim = -1)
        return features
    
class ResnetMLP(torch.nn.Module):
    def __init__(self, features_dim=1024, v='18', mlp_dim=1024):
        
        super(ResnetMLP, self).__init__()
        self.backbone = getattr(models, f'resnet{v}')(pretrained=True)
        backbone_dim = self.backbone.fc.in_features
        self.backbone.fc = nn.Identity()
    
        self.mlp = nn.Sequential(
                    nn.Flatten(),
                    nn.Linear(backbone_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
        )
        self.head = nn.Sequential(
                    
                    nn.Linear(mlp_dim, mlp_dim//4),
                    nn.BatchNorm1d(mlp_dim//4),
                    nn.ReLU(),
                    nn.Linear(mlp_dim//4, features_dim),
        )
    def backbone_mlp(self, x):
        x = self.backbone(x)
        x = self.mlp(x)
        return x
    
    def forward(self, x):
        x = self.backbone(x)
        x = self.mlp(x)
        x = self.head(x)
        x = F.normalize(x, dim = -1)
        return x
    
class MobilenetMLP(torch.nn.Module):
    def __init__(self, features_dim=1024, v='v2', mlp_dim=1024):
        
        super(MobilenetMLP, self).__init__()
        self.backbone = torch.hub.load('pytorch/vision:v0.6.0', f'mobilenet_{v}', pretrained=True)
        backbone_dim = self.backbone.classifier[1].in_features
        self.backbone = nn.Sequential(self.backbone.features, nn.AdaptiveAvgPool2d(output_size=(1, 1)))
    
        self.mlp = nn.Sequential(
                    nn.Flatten(),
                    nn.Linear(backbone_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
        )
        self.head = nn.Sequential(
                    
                    nn.Linear(mlp_dim, mlp_dim//4),
                    nn.BatchNorm1d(mlp_dim//4),
                    nn.ReLU(),
                    nn.Linear(mlp_dim//4, features_dim),
        )
    def backbone_mlp(self, x):
        x = self.backbone(x)
        x = self.mlp(x)
        return x
    
    def forward(self, x):
        x = self.backbone(x)
        x = self.mlp(x)
        x = self.head(x)
        x = F.normalize(x, dim = -1)
        return x

class ViTMLP(torch.nn.Module):
    def __init__(self, features_dim=1024, v=None, mlp_dim=1024, img_size=150, patch_size=25):
        
        super(ViTMLP, self).__init__()
        self.backbone = model = ViT(
            image_size = img_size,
            patch_size = patch_size,
            num_classes = 1000,
            dim = features_dim,
            depth = 6,
            heads = 8,
            mlp_dim = features_dim
        )
        backbone_dim = features_dim
        self.backbone.mlp_head = torch.nn.Identity()
        self.mlp = nn.Sequential(
                    nn.Flatten(),
                    nn.Linear(backbone_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
        )
        self.head = nn.Sequential(
                    
                    nn.Linear(mlp_dim, mlp_dim//4),
                    nn.BatchNorm1d(mlp_dim//4),
                    nn.ReLU(),
                    nn.Linear(mlp_dim//4, features_dim),
        )
    def backbone_mlp(self, x):
        x = self.backbone(x)
        x = self.mlp(x)
        return x
    
    def forward(self, x):
        x = self.backbone(x)
        x = self.mlp(x)
        x = self.head(x)
        x = F.normalize(x, dim = -1)
        return x

import timm

class TimmModel(torch.nn.Module):
    def __init__(self, v='tf_efficientnetv2_m_in21ft1k', out_features=15):     
        super(TimmModel, self).__init__()
        self.model = timm.create_model(v, pretrained=True)
        self.model.classifier = torch.nn.Linear(self.model.classifier.in_features, out_features=out_features)
    
    def forward(self, x):
        x = self.model(x)
        return x