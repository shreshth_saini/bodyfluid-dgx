import itertools
import math
import torch
import torchvision
from torch import nn
from torch.nn import functional as F
import torchvision
from torch.utils.data import DataLoader, Dataset
import albumentations as alb
import albumentations
from albumentations import Compose, RandomCrop, Normalize, HorizontalFlip, Resize 
from albumentations.pytorch import ToTensor
from skimage import io
from torch import optim
import glob 
import numpy as np
import os
from scheduler import CycleScheduler, ReduceLROnPlateauWithBacktrack
from tqdm.notebook import tqdm, trange
from pycm import ConfusionMatrix
import math
import torch
import torchvision
from torch import nn
from torch.nn import functional as F
import torchvision
from torch.optim.lr_scheduler import ReduceLROnPlateau, StepLR, LambdaLR
import datautils
from datautils import get_train_dl, get_test_dl, create_unlabeled_loader, get_psuedolabled_dl, get_train_dataset, get_dl_from_ds
import random
from fmix import FMix
import random 
from numpy.random import choice

try:
    from apex import amp
    APEX_AVAILABLE = True
except ModuleNotFoundError:
    APEX_AVAILABLE = False
    
    
# from tqdm.notebook import tqdm_notebook  # from tqdm import tqdm_notebook, trange
disable_tqdm = False
def fit_train(model, train_dl, test_dl, loss_fn, optim, epoch, scheduler, scheduler_step, scheduler_step_metric, aug_scheduler, args, warm_up, save_best_model=True, use_amp=False):
#               save_model_dir='data/')#, v='', eval_on_train=True, gradient_accumulation=1):
    gradient_accumulation = args['n_gradient_accumulation']
    eval_on_train = args['eval_on_train']
    v = args['v']
    save_model_dir = args['save_model_dir']
    cm_train_all = []
    loss_train_all = []
    cm_test_all = []
    loss_test_all = []
    lrs = []
    min_loss = 99999999999999
    max_metric = 0
    n_iters = len(train_dl)
#     train_dl = zip(train_dl, [1]*len(train_dl))
    train_ds = None
    fmix = FMix()
    
    if aug_scheduler:
        aug_p_multiplier = aug_scheduler.get_p()
    else:
        aug_p_multiplier = 1
        
    if warm_up:
        print('stepping scheduler for warmup')
        scheduler.step(None)
        warm_up = False
    
    for i in trange(epoch):
        best_model = False
        model.train()
        y_true_list = []
        y_pred_list = []
        total_loss  = 0
        t = tqdm(iter(train_dl), total=len(train_dl), disable=disable_tqdm)
        display_mix_up = True
        display_fmix = True 
        display_cutmix = True
        
        for iters, (X, y_true, weight) in enumerate(t):
            X = X.cuda()
            
            y_true = y_true.cuda()
            y_true = smooth_label(args, y_true)
            
            if not getattr(scheduler, 'warmup_phase', False):
                aug_dist = args['aug_dist']
                aug = choice(args['aug_choice'], 1, p=aug_dist)
            else:
                aug = 'no_aug'
                
            if aug == 'mixup':
#                 print('mixing-up')
                X_m, y_true_a, y_true_b, lam =  mixup_data(X, y_true, alpha=args['mix_up_alpha'] * aug_p_multiplier, use_cuda=True)
                if display_mix_up:
                    display_mixup_on_tb(args['tb'], X_m, 'mix_up')
                    display_mix_up = False
                    
                y_pred = model(X_m)
                loss = weight.numpy().tolist()[0]*mixup_criterion(loss_fn, y_pred, y_true_a, y_true_b, lam)
            
            elif aug == 'cutmix':
#                 print('cutmix-up')
                X_m, y_true_a, y_true_b, lam =  cutmix(X, y_true, alpha=args['cutmix_alpha'] * aug_p_multiplier)
                if display_cutmix:
                    display_mixup_on_tb(args['tb'], X_m, 'cutmix')
                    display_cutmix = False
                    
                y_pred = model(X_m)
                loss = weight.numpy().tolist()[0]*mixup_criterion(loss_fn, y_pred, y_true_a, y_true_b, lam)
            
            elif aug == 'fmix':
#                 print('f-mixing-up')
                X_m, y_true_a, y_true_b, lam =  fmix(X, y_true, alpha=args['fmix_alpha'] * aug_p_multiplier)
                if display_fmix:
                    display_mixup_on_tb(args['tb'], X_m, 'fmix')
                    display_fmix = False
                    
                y_pred = model(X_m)
                loss = weight.numpy().tolist()[0]*mixup_criterion(loss_fn, y_pred, y_true_a, y_true_b, lam)

            elif aug == 'noise_aug':
#                 print('noise_aug')
                X_m, y_true_a, y_true_b, lam =  generatenoisy_data(X, y_true, alpha=args['noise_alpha'] * aug_p_multiplier, use_cuda=True)
                y_pred = model(X_m)
                loss = weight.numpy().tolist()[0]*mixup_criterion(loss_fn, y_pred, y_true_a, y_true_b, lam)
                
            elif aug == 'no_aug':
#                 print('no aug')  
                y_pred = model(X)
                loss = weight.numpy().tolist()[0]*loss_fn(y_pred, y_true)
            
            else:
                raise Exception('Invalid augmentation technique', aug)
                
            if use_amp:
                with amp.scale_loss(loss, optim) as scaled_loss:
                    scaled_loss.backward()
            else:
                loss.backward()
            
            if scheduler:
                if scheduler_step=='minibatch':
                    scheduler.step()
                            
            total_loss += loss.item()
            if (iters + 1) % gradient_accumulation == 0:
                optim.step()
                optim.zero_grad()
        
            if not disable_tqdm:
                t.set_description(f'Epoch {i}: loss: {round(total_loss, 2)} lr: '+'%.2E'%(optim.param_groups[0]['lr']))
#                 print('\r', f'Epoch {i}: loss: {round(total_loss, 2)} {iters}/{n_iters}', end='')
            if iters % 500 == 0:
                for param_group in optim.param_groups:
                    lrs.append(param_group['lr'])
        
        if eval_on_train:
            cm_train, avg_loss_train = test(model, train_dl, loss_fn, dataset='train')
            save_reports(cm_train, args['class_names'], save_model_dir, v, i, dataset='train', avg_loss=avg_loss_train)
        else:
            cm_train, avg_loss_train = None, None
        cm_test, avg_loss_test = test(model, test_dl, loss_fn, dataset='test')
        save_reports(cm_test, args['class_names'], save_model_dir, v, i, dataset='test', avg_loss=avg_loss_test)
        
        cm_train_all.append(cm_train)
        loss_train_all.append(avg_loss_train)
        cm_test_all.append(cm_test)
        loss_test_all.append(avg_loss_test)
          
        
                
        if scheduler:
            if scheduler_step=='epoch':
                if scheduler_step_metric=='acc':
                    print('stepping on acc: ' +  str(cm_test.Overall_ACC))
                    scheduler.step(cm_test.Overall_ACC)
                elif scheduler_step_metric=='loss':
                    print('stepping on loss: ' +  str(avg_loss_test))
                    scheduler.step(avg_loss_test)
                else:
                    print('stepping on None')
                    scheduler.step()
        if aug_scheduler:
            if scheduler_step_metric=='acc':
                aug_p_multiplier = aug_scheduler.step_and_get_p(cm_test.Overall_ACC, optim.param_groups[0]['lr'])
            elif scheduler_step_metric=='loss':
                aug_p_multiplier = aug_scheduler.step_and_get_p(avg_loss_test, optim.param_groups[0]['lr'])
            else:
                print('Augscheduler - stepping on None')
                aug_p_multiplier= aug_scheduler.step_and_get_p()
        
        if save_best_model:
            if max_metric <= cm_test.Overall_ACC:
                torch.save({'epoch': i,
                            'acc': cm_test.Overall_ACC,
                            'model_weight': model.state_dict(),
                            'optim_weight': optim.state_dict()}, open(save_model_dir + 'model_v'+ v +'_state_dict_best_acc'+'.pt', 'wb'))
                print('saving model for best acc : %f previous best %f' % (cm_test.Overall_ACC, max_metric))
                max_metric = cm_test.Overall_ACC
                best_model=True
            if min_loss >= avg_loss_test:
                torch.save({'epoch': i,
                            'acc': cm_test.Overall_ACC,
                            'model_weight': model.state_dict(),
                            'optim_weight': optim.state_dict()}, open(save_model_dir + 'model_v'+ v +'_state_dict_best_loss'+'.pt', 'wb'))
                print('saving model for best loss : %f previous best %f' % (avg_loss_test, min_loss))
                min_loss = avg_loss_test
                
        else:
           
            torch.save({'epoch': i,
                            'acc': cm_test.Overall_ACC,
                            'model_weight': model.state_dict(),
                            'optim_weight': optim.state_dict()},
                       open(save_model_dir + 'model'+ v +'_state_dict_e'+ str(i) +'.pt', 'wb'))
        

#         if i % 10 == 0:
#                 torch.save({'epoch': i,
#                             'acc': cm_test.Overall_ACC,
#                             'model_weight': model.state_dict(),
#                             'optim_weight': optim.state_dict()}
#                            , open(save_model_dir + 'model_v'+ v +'_state_dict_e'+ str(i) +'.pt', 'wb'))
        torch.save({'epoch': i,
                            'acc': cm_test.Overall_ACC,
                            'model_weight': model.state_dict(),
                            'optim_weight': optim.state_dict()}
                           , open(save_model_dir + 'model_v'+ v +'_curr_model.pt', 'wb'))
        if args.get('tb'):
            tb = args['tb']
            save_reports_to_tb(cm_test, args['class_names'], save_model_dir, v, i, dataset='test', avg_loss=avg_loss_test, tb=tb, lr=optim.param_groups[0]['lr'], p_multiplier=aug_p_multiplier, best_model=best_model)

        
        # if given unlabeled_data_filepaths 
        if args.get('unlabeled_data_filepaths') and i % args['psuedo_label_n_intervals'] == 0:
            new_data_fp, new_data_label = psuedolabel(args['unlabeled_data_filepaths'], model, args['classes'], args['psuedolabel_confidence_threshold'])
            
            if len(new_data_fp) == 0:
                print('no psuedolabels added - maybe try decreasing the value of threshold %f' % (args['psuedolabel_confidence_threshold']))
                continue
            
            if train_ds is None:
                train_ds = get_train_dataset(args['train_filepaths'], class_names=args['class_names'])
            ps_ds = get_train_dataset(new_data_fp, labels=new_data_label.astype(float),
                                      class_names=args['class_names'], weight=args['psuedo_label_weight'])
#             print(ps_ds[0], train_ds[0])
            train_ds = torch.utils.data.ConcatDataset([train_ds, ps_ds])
            train_dl = get_dl_from_ds(train_ds)
            

            args['unlabeled_data_filepaths'] = list(set(args['unlabeled_data_filepaths']) - set(new_data_fp)) 
            report_psl = '\nEpoch %d, added %d, %d data using psudeo-labeling count: %s, unlabeled data remaining %d' % (i, len(new_data_fp),len(new_data_label), get_class_counts(new_data_label, class_names=args['class_names']),
                                                                  len(args['unlabeled_data_filepaths']))
            print(report_psl)
            save_filepath_label(new_data_fp, new_data_label,
                                class_names=args['class_names'],
                                epoch=i,
                                filename=save_model_dir + 'pseudolabel_preds' + '_v' + v + '.txt')
            txt_filename = save_model_dir + 'pseudolabel_report' + '_v' + v + '.txt'
            open(txt_filename, 'a').write(report_psl)
           
    return cm_train_all, loss_train_all, cm_test_all, loss_test_all, lrs

# @torch.no_grad()
# def test(model, dl, loss_fn, dataset):
#     loss_all = []
#     y_true_list = []
#     y_pred_list = []
#     total_loss  = 0
#     model.eval()
#     cm = None
#     t = tqdm(iter(dl), leave=False, total=len(dl), disable=disable_tqdm)
    
#     for X, y_true, _ in t:
#         X = X.cuda()
#         y_true = y_true.cuda()
        
#         y_pred, y_pred_arc = model(X, return_arc_margin=True)
# #         print(y_true)
    
#         loss = loss_fn((y_pred, y_pred_arc), y_true,)
# #         y_pred = y_pred[1]
#         y_true = torch.argmax(y_true, dim=-1)
#         y_true_list.extend(y_true.cpu().numpy())
#         y_pred_list.extend(torch.argmax(y_pred, dim=-1).detach().cpu().numpy())
#         total_loss += loss.item()
#         if not disable_tqdm:
#             t.set_description(f'eval on {dataset} total_loss: {round(total_loss, 2)}')
            
# #         print(y_true_list, y_pred_list)
# #         break
#     cm = ConfusionMatrix(actual_vector=y_true_list, predict_vector=y_pred_list)
#     avg_loss = total_loss/len(dl)
# #     print(f'{dataset} accuracy score {cm.Overall_ACC} f1 score {cm.F1_Micro} avg_loss {avg_loss}')
# #     print(f'{dataset} avg_loss {avg_loss}')

#     return cm, avg_loss
@torch.no_grad()
def test(model, dl, loss_fn, dataset):
    loss_all = []
    y_true_list = []
    y_pred_list = []
    total_loss  = 0
    model.eval()
    cm = None
    t = tqdm(iter(dl), leave=False, total=len(dl), disable=disable_tqdm)
    
    for X, y_true, _ in t:
        X = X.cuda()
        y_true = y_true.cuda()
        
        y_pred = model(X)
#         print(y_true)
    
        loss = loss_fn(y_pred, y_true,)
#         y_pred = y_pred[1]
        y_true = torch.argmax(y_true, dim=-1)
        y_true_list.extend(y_true.cpu().numpy())
        y_pred_list.extend(torch.argmax(y_pred, dim=-1).detach().cpu().numpy())
        total_loss += loss.item()
        if not disable_tqdm:
            t.set_description(f'eval on {dataset} total_loss: {round(total_loss, 2)}')
            
#         print(y_true_list, y_pred_list)
#         break
    cm = ConfusionMatrix(actual_vector=y_true_list, predict_vector=y_pred_list)
    avg_loss = total_loss/len(dl)
#     print(f'{dataset} accuracy score {cm.Overall_ACC} f1 score {cm.F1_Micro} avg_loss {avg_loss}')
#     print(f'{dataset} avg_loss {avg_loss}')

    return cm, avg_loss

    
def save_reports_to_tb(cm, classes, save_model_dir, v, epoch, dataset='', avg_loss='', tb='', lr='', p_multiplier='', best_model=False):
    print('hello')
    cm_filename = save_model_dir + 'cm_' + dataset + '_v' + v + '_e' + str(epoch) +'.png'
    fig = pretty_plot_confusion_matrix(pd.DataFrame(pd.DataFrame(cm.matrix).values.T,  columns=[classes[idx] for idx in cm.classes],
                                          index=[classes[idx] for idx in cm.classes]), figsize=(15, 15),)
    fig.savefig(cm_filename, dpi=fig.dpi)

    tb.add_scalar(dataset+'Accuracy', cm.Overall_ACC, epoch)
    tb.add_scalar(dataset+'F1_macro', cm.F1_Macro, epoch)
    tb.add_scalar(dataset+'F1_micro', cm.F1_Micro, epoch)
    tb.add_scalar(dataset+'Lr', lr, epoch)
    tb.add_scalar(dataset+'Aug_Alpha_PMultiplier', p_multiplier, epoch)
    tb.add_image(dataset+'Confusion Matrix', io.imread(cm_filename).transpose(2, 0, 1), global_step=epoch)
    tb.add_scalar(dataset+'Avg Loss', avg_loss, epoch)
    if best_model:
        tb.add_image(dataset + 'Confusion Matrix Best Acc', io.imread(cm_filename).transpose(2, 0, 1), global_step=epoch)
    tb.close()

@torch.no_grad()
def smooth_label(args, Y):
    if len(Y.shape) == 2:
        Y = torch.argmax(Y, dim=-1)
    nY = nn.functional.one_hot(Y, args['classes']).float()
    nY += args['label_smoothing'] / (args['classes'] - 1)
    nY[range(Y.size(0)), Y] -= args['label_smoothing'] / (args['classes'] - 1) + args['label_smoothing']
    return nY

import pandas as pd
from confusion_matrix_pretty_print import plot_confusion_matrix_from_data, pretty_plot_confusion_matrix

def save_reports(cm, classes, save_model_dir, v, epoch, dataset='', avg_loss=''):
#     save_reports(cm_test, args['class_names'], filename=save_model_dir + 'report'+ v +'_test_cm_e_' + str(i) +'.png')
    cm_filename = save_model_dir + 'cm_' + dataset + '_v' + v + '_e' + str(epoch) +'.png'
    txt_filename = save_model_dir + 'report_' + dataset + '_v' + v + '.txt'
    txt_file = open(txt_filename, 'a')
    fig = pretty_plot_confusion_matrix(pd.DataFrame(pd.DataFrame(cm.matrix).values.T,  columns=[classes[idx] for idx in cm.classes],
                                          index=[classes[idx] for idx in cm.classes]), figsize=(15, 15),)
    report = f'\nepoch {epoch} {dataset} accuracy score {cm.Overall_ACC} f1 score {cm.F1_Micro} avg_loss {avg_loss}'
    print(report)
    txt_file.write(report)
    txt_file.close()
    fig.savefig(cm_filename, dpi=fig.dpi)

@torch.no_grad()
def if_confident(y_pred, psuedolabel_confidence_threshold):
    confidence = y_pred.max(dim=-1).values >= psuedolabel_confidence_threshold
    return confidence.view(-1)
    
    
    
@torch.no_grad()
def psuedolabel(unlabeled_filepaths, model, n_classes, psuedolabel_confidence_threshold, get_pred_vectors=False):
#     unlabeled_filepaths = np.array(unlabeled_filepaths)
    dl = create_unlabeled_loader(unlabeled_filepaths)
    new_data_fp = []
    new_data_label = []
    new_data_vector = []
    t = tqdm(iter(dl), leave=False, total=len(dl), disable=disable_tqdm)
    if not disable_tqdm:
        t.set_description(f'psuedo-labelling unlabeled dataset')
    model.eval()
    for X, filepaths in t:
        filepaths = np.array(filepaths)
        X = X.cuda()
        y_pred = model(X, return_arc_margin=False)
        y_pred = torch.softmax(y_pred, dim=-1)
        confident = if_confident(y_pred, psuedolabel_confidence_threshold)
        confident_np = confident.detach().cpu().numpy().astype(bool)

        confident_filepaths = filepaths[confident_np]
        confident_pred = y_pred[confident.nonzero(), :]
        if confident_pred.shape[0] == 0:
            continue
        if get_pred_vectors:
            new_data_vector.extend(confident_pred.squeeze(1).detach().cpu().numpy())
        y_label = F.one_hot(torch.argmax(confident_pred, dim=-1), num_classes=n_classes)
#         zip(confident_filepaths, y_label.detach().cpu(),numpy())
        new_data_fp.extend(confident_filepaths)
        new_data_label.extend(y_label.squeeze(1).detach().cpu().numpy())
    new_data_label = np.array(new_data_label)
#     print(new_data_label, new_data_label.shape)
    if get_pred_vectors:
        return new_data_fp, new_data_label, new_data_vector 
    return new_data_fp, new_data_label

def save_filepath_label(new_data_fp, new_data_label,
                                class_names, epoch,
                                filename):
#     file = open(filename, 'a')
    class_names = list(class_names)
    class_preds = [class_names[x] for x in np.argmax(new_data_label, axis=-1).flatten()]
    open(filename, 'a').writelines([f'{epoch}\t{filepath}\t{label}\n' for filepath, label in zip(new_data_fp, class_preds)])
    
from collections import Counter
def get_class_counts(labels, class_names):
    class_names = list(class_names)
    return Counter([class_names[x] 
         for x in np.argmax(labels, axis=-1).flatten()])
#     print('added %d ')


@torch.no_grad()
def get_pred_df(unlabeled_filepaths, model, class_names, true_label=None):
#     unlabeled_filepaths = np.array(unlabeled_filepaths)
    dl = create_unlabeled_loader(unlabeled_filepaths)
    new_data_fp = []
    new_data_label = []
    new_data_vector = []
    n_classes = len(class_names)
    t = tqdm(iter(dl), leave=False, total=len(dl), disable=disable_tqdm)
    if not disable_tqdm:
        t.set_description(f'predicting dataset')
    model.eval()
    for X, filepaths in t:
        filepaths = np.array(filepaths)
        X = X.cuda()
        y_pred = model(X, return_arc_margin=False)
        y_pred = torch.softmax(y_pred, dim=-1)
        new_data_vector.extend(y_pred.squeeze(1).detach().cpu().numpy())
        y_label = F.one_hot(torch.argmax(y_pred, dim=-1), num_classes=n_classes)
        new_data_fp.extend(filepaths)
        new_data_label.extend(y_label.squeeze(1).detach().cpu().numpy())
    new_data_label = np.array(new_data_label)
    df = pd.DataFrame(zip(new_data_fp, new_data_label, new_data_vector), columns=['fp', 'pred_ohe', 'pred_vector'])
    df['pred_class'] = df['pred_ohe'].apply(lambda x: class_names[int(np.argmax(x))])
    df['pred_proba'] = df['pred_vector'].apply(lambda x: max(x))
    if true_label:
        df['true_label'] = true_label
        df['misclassified'] = df['true_label'] != df['pred_class']
    return df


def get_label_from_path(path):
    splits = re.split('/|\\\\', path)
    return splits[-2] 


def get_cosine_schedule_with_warmup(optimizer,
                                    num_warmup_steps,
                                    num_training_steps,
                                    num_cycles=7./16.,
                                    last_epoch=-1):
    def _lr_lambda(current_step):
        if current_step < num_warmup_steps:
            return float(current_step) / float(max(1, num_warmup_steps))
        no_progress = float(current_step - num_warmup_steps) / \
            float(max(1, num_training_steps - num_warmup_steps))
        return max(0., math.cos(math.pi * num_cycles * no_progress))

    return LambdaLR(optimizer, _lr_lambda, last_epoch)


def display_image_and_model_on_tb(tb, train_dl, model):
    class UnNormalize(object):
        def __init__(self, mean, std):
            self.mean = mean
            self.std = std

        def __call__(self, tensor):
            """
            Args:
                tensor (Tensor): Tensor image of size (C, H, W) to be normalized.
            Returns:
                Tensor: Normalized image.
            """
            for t, m, s in zip(tensor, self.mean, self.std):
                t.mul_(s).add_(m)
                # The normalize code -> t.sub_(m).div_(s)
            return tensor
    inv = UnNormalize( mean=datautils.mean,
                         std=datautils.std)
    imgs, labels, _ = next(iter(train_dl))
    grid = torchvision.utils.make_grid([inv(x) for x in imgs])
    tb.add_image('images', grid)
    tb.add_graph(model, imgs.cuda())
    tb.close()
    
def display_mixup_on_tb(tb, imgs, tech):
    class UnNormalize(object):
        def __init__(self, mean, std):
            self.mean = mean
            self.std = std

        def __call__(self, tensor):
            """
            Args:
                tensor (Tensor): Tensor image of size (C, H, W) to be normalized.
            Returns:
                Tensor: Normalized image.
            """
            for t, m, s in zip(tensor, self.mean, self.std):
                t.mul_(s).add_(m)
                # The normalize code -> t.sub_(m).div_(s)
            return tensor
    inv = UnNormalize(mean=datautils.mean,
                         std=datautils.std)

    grid = torchvision.utils.make_grid([inv(x) for x in imgs])
    tb.add_image('%s_images' % (tech), grid)
    tb.close()

def plot_confusion_matrix(cm, classes):
    fig = pretty_plot_confusion_matrix(pd.DataFrame(pd.DataFrame(cm.matrix).values.T,  columns=classes,
                                          index=classes), figsize=(15, 15))
    return fig

def generatenoisy_data(x, y, alpha=1.0, use_cuda=True):
    '''Returns mixed inputs, pairs of targets, and lambda'''
    if alpha > 0:
        lam = np.random.beta(alpha, alpha)
    else:
        lam = 1

    batch_size = x.size()[0]
    if use_cuda:
        index = torch.randperm(batch_size).cuda()
    else:
        index = torch.randperm(batch_size)
        
    y_a, y_b = y, y[index]
    return x, y_a, y_b, lam

    
    
def mixup_data(x, y, alpha=1.0, use_cuda=True):
    '''Returns mixed inputs, pairs of targets, and lambda'''
    if alpha > 0:
        lam = np.random.beta(alpha, alpha)
    else:
        lam = 1

    batch_size = x.size()[0]
    if use_cuda:
        index = torch.randperm(batch_size).cuda()
    else:
        index = torch.randperm(batch_size)

    mixed_x = lam * x + (1 - lam) * x[index, :]
    y_a, y_b = y, y[index]
    return mixed_x, y_a, y_b, lam


def mixup_criterion(criterion, pred, y_a, y_b, lam):
    return lam * criterion(pred, y_a) + (1 - lam) * criterion(pred, y_b)

def rand_bbox(size, lam):
    W = size[2]
    H = size[3]
    cut_rat = np.sqrt(1. - lam)
    cut_w = np.int(W * cut_rat)
    cut_h = np.int(H * cut_rat)

    # uniform
    cx = np.random.randint(W)
    cy = np.random.randint(H)

    bbx1 = np.clip(cx - cut_w // 2, 0, W)
    bby1 = np.clip(cy - cut_h // 2, 0, H)
    bbx2 = np.clip(cx + cut_w // 2, 0, W)
    bby2 = np.clip(cy + cut_h // 2, 0, H)

    return bbx1, bby1, bbx2, bby2

def cutmix(input, target, alpha):
    lam = np.random.beta(alpha, alpha)
    rand_index = torch.randperm(input.size()[0]).cuda()
    target_a = target
    target_b = target[rand_index]
    bbx1, bby1, bbx2, bby2 = rand_bbox(input.size(), lam)
    input[:, :, bbx1:bbx2, bby1:bby2] = input[rand_index, :, bbx1:bbx2, bby1:bby2]
            # adjust lambda to exactly match pixel ratio
    lam = 1 - ((bbx2 - bbx1) * (bby2 - bby1) / (input.size()[-1] * input.size()[-2]))
    return input, target_a, target_b, lam

@torch.no_grad()
def get_pred_df_and_feature_vectors(unlabeled_filepaths, model, class_names, true_label=None, eval_on_arc_margin=False):
#     unlabeled_filepaths = np.array(unlabeled_filepaths)
    dl = create_unlabeled_loader(unlabeled_filepaths)
    new_data_fp = []
    new_data_label = []
    new_data_vector = []
    new_data_feature_vectors = []
    n_classes = len(class_names)
    t = tqdm(iter(dl), leave=False, total=len(dl), disable=disable_tqdm)
    if not disable_tqdm:
        t.set_description(f'predicting dataset')
    model.eval()
    for X, filepaths in t:
        filepaths = np.array(filepaths)
        X = X.cuda()
        if eval_on_arc_margin:
            _, y_pred = model(X, return_arc_margin=True)
        else:
            y_pred = model(X, return_arc_margin=False)
        
        y_features = model.embed(X)
        y_pred = torch.softmax(y_pred, dim=-1)
        new_data_vector.extend(y_pred.squeeze(1).detach().cpu().numpy())
        y_label = F.one_hot(torch.argmax(y_pred, dim=-1), num_classes=n_classes)
        new_data_fp.extend(filepaths)
        new_data_label.extend(y_label.squeeze(1).detach().cpu().numpy())
        new_data_feature_vectors.extend(y_features.detach().cpu().numpy())
    new_data_label = np.array(new_data_label)
    df = pd.DataFrame(zip(new_data_fp, new_data_label, new_data_vector, new_data_feature_vectors), columns=['fp', 'pred_ohe', 'pred_vector', 'pred_feature_vector'])
    df['pred_class'] = df['pred_ohe'].apply(lambda x: class_names[int(np.argmax(x))])
    df['pred_proba'] = df['pred_vector'].apply(lambda x: max(x)) 
    if true_label:
        df['true_label'] = true_label
        df['misclassified'] = df['true_label'] != df['pred_class']
    return df

@torch.no_grad()
def get_pred_df_efficientnet(unlabeled_filepaths, model, class_names, true_label=None, batch_size=datautils.batch_size):
#     unlabeled_filepaths = np.array(unlabeled_filepaths)
    dl = create_unlabeled_loader(unlabeled_filepaths, batch_size=batch_size)
    new_data_fp = []
    new_data_label = []
    new_data_vector = []
    n_classes = len(class_names)
    t = tqdm(iter(dl), leave=False, total=len(dl), disable=disable_tqdm)
    if not disable_tqdm:
        t.set_description(f'predicting dataset')
    model.eval()
    for X, filepaths in t:
        filepaths = np.array(filepaths)
        X = X.cuda()
        
        y_pred = model(X,)
        
        y_pred = torch.softmax(y_pred, dim=-1)
        new_data_vector.extend(y_pred.squeeze(1).detach().cpu().numpy())
        y_label = F.one_hot(torch.argmax(y_pred, dim=-1), num_classes=n_classes)
        new_data_fp.extend(filepaths)
        new_data_label.extend(y_label.squeeze(1).detach().cpu().numpy())
        
    new_data_label = np.array(new_data_label)
    df = pd.DataFrame(zip(new_data_fp, new_data_label, new_data_vector, ), columns=['fp', 'pred_ohe', 'pred_vector', ])
    df['pred_class'] = df['pred_ohe'].apply(lambda x: class_names[int(np.argmax(x))])
    df['pred_proba'] = df['pred_vector'].apply(lambda x: max(x)) 
    if true_label:
        df['true_label'] = true_label
        df['misclassified'] = df['true_label'] != df['pred_class']
    return df


@torch.no_grad()
def get_pred_df_wihtout_softmax(unlabeled_filepaths, model, class_names, true_label=None, batch_size=datautils.batch_size):
#     unlabeled_filepaths = np.array(unlabeled_filepaths)
    dl = create_unlabeled_loader(unlabeled_filepaths, batch_size=batch_size)
    new_data_fp = []
    new_data_label = []
    new_data_vector = []
    n_classes = len(class_names)
    t = tqdm(iter(dl), leave=False, total=len(dl), disable=disable_tqdm)
    if not disable_tqdm:
        t.set_description(f'predicting dataset')
    model.eval()
    for X, filepaths in t:
        filepaths = np.array(filepaths)
        X = X.cuda()
        
        y_pred = model(X,)
        
#         y_pred = torch.softmax(y_pred, dim=-1)
        new_data_vector.extend(y_pred.squeeze(1).detach().cpu().numpy())
        y_label = F.one_hot(torch.argmax(y_pred, dim=-1), num_classes=n_classes)
        new_data_fp.extend(filepaths)
        new_data_label.extend(y_label.squeeze(1).detach().cpu().numpy())
        
    new_data_label = np.array(new_data_label)
    df = pd.DataFrame(zip(new_data_fp, new_data_label, new_data_vector, ), columns=['fp', 'pred_ohe', 'pred_vector', ])
    df['pred_class'] = df['pred_ohe'].apply(lambda x: class_names[int(np.argmax(x))])
    df['pred_proba'] = df['pred_vector'].apply(lambda x: max(x)) 
    if true_label:
        df['true_label'] = true_label
        df['misclassified'] = df['true_label'] != df['pred_class']
    return df