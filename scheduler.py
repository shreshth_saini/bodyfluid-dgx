from math import cos, pi, floor, sin

from torch.optim import lr_scheduler
import torch

class CosineLR(lr_scheduler._LRScheduler):
    def __init__(self, optimizer, lr_min, lr_max, step_size):
        self.lr_min = lr_min
        self.lr_max = lr_max
        self.step_size = step_size
        self.iteration = 0

        super().__init__(optimizer, -1)

    def get_lr(self):
        lr = self.lr_min + 0.5 * (self.lr_max - self.lr_min) * (
            1 + cos(self.iteration / self.step_size * pi)
        )
        self.iteration += 1

        if self.iteration == self.step_size:
            self.iteration = 0

        return [lr for base_lr in self.base_lrs]


class PowerLR(lr_scheduler._LRScheduler):
    def __init__(self, optimizer, lr_min, lr_max, warmup):
        self.lr_min = lr_min
        self.lr_max = lr_max
        self.warmup = warmup
        self.iteration = 0

        super().__init__(optimizer, -1)

    def get_lr(self):
        if self.iteration < self.warmup:
            lr = (
                self.lr_min + (self.lr_max - self.lr_min) / self.warmup * self.iteration
            )

        else:
            lr = self.lr_max * (self.iteration - self.warmup + 1) ** -0.5

        self.iteration += 1

        return [lr for base_lr in self.base_lrs]


class SineLR(lr_scheduler._LRScheduler):
    def __init__(self, optimizer, lr_min, lr_max, step_size):
        self.lr_min = lr_min
        self.lr_max = lr_max
        self.step_size = step_size
        self.iteration = 0

        super().__init__(optimizer, -1)

    def get_lr(self):
        lr = self.lr_min + (self.lr_max - self.lr_min) * sin(
            self.iteration / self.step_size * pi
        )
        self.iteration += 1

        if self.iteration == self.step_size:
            self.iteration = 0

        return [lr for base_lr in self.base_lrs]


class LinearLR(lr_scheduler._LRScheduler):
    def __init__(self, optimizer, lr_min, lr_max, warmup, step_size):
        self.lr_min = lr_min
        self.lr_max = lr_max
        self.step_size = step_size
        self.warmup = warmup
        self.iteration = 0

        super().__init__(optimizer, -1)

    def get_lr(self):
        if self.iteration < self.warmup:
            lr = self.lr_max

        else:
            lr = self.lr_max + (self.iteration - self.warmup) * (
                self.lr_min - self.lr_max
            ) / (self.step_size - self.warmup)
        self.iteration += 1

        if self.iteration == self.step_size:
            self.iteration = 0

        return [lr for base_lr in self.base_lrs]


class CLR(lr_scheduler._LRScheduler):
    def __init__(self, optimizer, lr_min, lr_max, step_size):
        self.epoch = 0
        self.lr_min = lr_min
        self.lr_max = lr_max
        self.current_lr = lr_min
        self.step_size = step_size

        super().__init__(optimizer, -1)

    def get_lr(self):
        cycle = floor(1 + self.epoch / (2 * self.step_size))
        x = abs(self.epoch / self.step_size - 2 * cycle + 1)
        lr = self.lr_min + (self.lr_max - self.lr_min) * max(0, 1 - x)
        self.current_lr = lr

        self.epoch += 1

        return [lr for base_lr in self.base_lrs]



from torch.optim.lr_scheduler import LambdaLR
class WarmupLinearSchedule(LambdaLR):
    """ Linear warmup and then linear decay.
        Linearly increases learning rate from 0 to 1 over `warmup_steps` training steps.
        Linearly decreases learning rate from 1. to 0. over remaining `t_total - warmup_steps` steps.
    """
    def __init__(self, optimizer, warmup_steps, last_epoch=-1):
        self.warmup_steps = warmup_steps
        self.t_total = t_total
        super(WarmupLinearSchedule, self).__init__(optimizer, self.lr_lambda, last_epoch=last_epoch)

    def lr_lambda(self, step):
        if step < self.warmup_steps:
            return float(step) / float(max(1, self.warmup_steps))
        raise Exception('steps exceeded warmup_steps')
    
# Copyright 2019 fastai

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Borrowed from https://github.com/fastai/fastai and changed to make it runs like PyTorch lr scheduler


class CycleAnnealScheduler:
    def __init__(
        self, optimizer, lr_max, lr_divider, cut_point, step_size, momentum=None
    ):
        self.lr_max = lr_max
        self.lr_divider = lr_divider
        self.cut_point = step_size // cut_point
        self.step_size = step_size
        self.iteration = 0
        self.cycle_step = int(step_size * (1 - cut_point / 100) / 2)
        self.momentum = momentum
        self.optimizer = optimizer

    def get_lr(self):
        if self.iteration > 2 * self.cycle_step:
            cut = (self.iteration - 2 * self.cycle_step) / (
                self.step_size - 2 * self.cycle_step
            )
            lr = self.lr_max * (1 + (cut * (1 - 100) / 100)) / self.lr_divider

        elif self.iteration > self.cycle_step:
            cut = 1 - (self.iteration - self.cycle_step) / self.cycle_step
            lr = self.lr_max * (1 + cut * (self.lr_divider - 1)) / self.lr_divider

        else:
            cut = self.iteration / self.cycle_step
            lr = self.lr_max * (1 + cut * (self.lr_divider - 1)) / self.lr_divider

        return lr

    def get_momentum(self):
        if self.iteration > 2 * self.cycle_step:
            momentum = self.momentum[0]

        elif self.iteration > self.cycle_step:
            cut = 1 - (self.iteration - self.cycle_step) / self.cycle_step
            momentum = self.momentum[0] + cut * (self.momentum[1] - self.momentum[0])

        else:
            cut = self.iteration / self.cycle_step
            momentum = self.momentum[0] + cut * (self.momentum[1] - self.momentum[0])

        return momentum

    def step(self):
        lr = self.get_lr()

        if self.momentum is not None:
            momentum = self.get_momentum()

        self.iteration += 1

        if self.iteration == self.step_size:
            self.iteration = 0

        for group in self.optimizer.param_groups:
            group['lr'] = lr

            if self.momentum is not None:
                group['betas'] = (momentum, group['betas'][1])

        return lr


def anneal_linear(start, end, proportion):
    return start + proportion * (end - start)


def anneal_cos(start, end, proportion):
    cos_val = cos(pi * proportion) + 1

    return end + (start - end) / 2 * cos_val


class Phase:
    def __init__(self, start, end, n_iter, anneal_fn):
        self.start, self.end = start, end
        self.n_iter = n_iter
        self.anneal_fn = anneal_fn
        self.n = 0

    def step(self):
        self.n += 1

        return self.anneal_fn(self.start, self.end, self.n / self.n_iter)

    def reset(self):
        self.n = 0

    @property
    def is_done(self):
        return self.n >= self.n_iter


class CycleScheduler:
    def __init__(
        self,
        optimizer,
        lr_max,
        n_iter,
        momentum=(0.95, 0.85),
        divider=25,
        warmup_proportion=0.3,
        phase=('linear', 'cos'),
    ):
        self.optimizer = optimizer

        phase1 = int(n_iter * warmup_proportion)
        phase2 = n_iter - phase1
        lr_min = lr_max / divider

        phase_map = {'linear': anneal_linear, 'cos': anneal_cos}

        self.lr_phase = [
            Phase(lr_min, lr_max, phase1, phase_map[phase[0]]),
            Phase(lr_max, lr_min / 1e4, phase2, phase_map[phase[1]]),
        ]

        self.momentum = momentum

        if momentum is not None:
            mom1, mom2 = momentum
            self.momentum_phase = [
                Phase(mom1, mom2, phase1, phase_map[phase[0]]),
                Phase(mom2, mom1, phase2, phase_map[phase[1]]),
            ]

        else:
            self.momentum_phase = []

        self.phase = 0

    def step(self):
        lr = self.lr_phase[self.phase].step()

        if self.momentum is not None:
            momentum = self.momentum_phase[self.phase].step()

        else:
            momentum = None

        for group in self.optimizer.param_groups:
            group['lr'] = lr

            if self.momentum is not None:
                if 'betas' in group:
                    group['betas'] = (momentum, group['betas'][1])

                else:
                    group['momentum'] = momentum

        if self.lr_phase[self.phase].is_done:
            self.phase += 1

        if self.phase >= len(self.lr_phase):
            for phase in self.lr_phase:
                phase.reset()

            for phase in self.momentum_phase:
                phase.reset()

            self.phase = 0

        return lr, momentum


class LRFinder(lr_scheduler._LRScheduler):
    def __init__(self, optimizer, lr_min, lr_max, step_size, linear=False):
        ratio = lr_max / lr_min
        self.linear = linear
        self.lr_min = lr_min
        self.lr_mult = (ratio / step_size) if linear else ratio ** (1 / step_size)
        self.iteration = 0
        self.lrs = []
        self.losses = []

        super().__init__(optimizer, -1)

    def get_lr(self):
        lr = (
            self.lr_mult * self.iteration
            if self.linear
            else self.lr_mult ** self.iteration
        )
        lr = self.lr_min + lr if self.linear else self.lr_min * lr

        self.iteration += 1
        self.lrs.append(lr)

        return [lr for base_lr in self.base_lrs]

    def record(self, loss):
        self.losses.append(loss)

    def save(self, filename):
        with open(filename, 'w') as f:
            for lr, loss in zip(self.lrs, self.losses):
                f.write('{},{}\n'.format(lr, loss))
                
# https://raw.githubusercontent.com/katsura-jp/pytorch-cosine-annealing-with-warmup/master/cosine_annearing_with_warmup.py

import math
from torch.optim.lr_scheduler import _LRScheduler

class CosineAnnealingWarmUpRestarts(_LRScheduler):
    def __init__(self, optimizer, T_0, T_mult=1, eta_max=0.1, T_up=0, gamma=1., last_epoch=-1):
        if T_0 <= 0 or not isinstance(T_0, int):
            raise ValueError("Expected positive integer T_0, but got {}".format(T_0))
        if T_mult < 1 or not isinstance(T_mult, int):
            raise ValueError("Expected integer T_mult >= 1, but got {}".format(T_mult))
        if T_up < 0 or not isinstance(T_up, int):
            raise ValueError("Expected positive integer T_up, but got {}".format(T_up))
        self.T_0 = T_0
        self.T_mult = T_mult
        self.base_eta_max = eta_max
        self.eta_max = eta_max
        self.T_up = T_up
        self.T_i = T_0
        self.gamma = gamma
        self.cycle = 0
        self.T_cur = last_epoch
        super(CosineAnnealingWarmUpRestarts, self).__init__(optimizer, last_epoch)
        
    
    def get_lr(self):
        if self.T_cur == -1:
            return self.base_lrs
        elif self.T_cur < self.T_up:
            return [(self.eta_max - base_lr)*self.T_cur / self.T_up + base_lr for base_lr in self.base_lrs]
        else:
            return [base_lr + (self.eta_max - base_lr) * (1 + math.cos(math.pi * (self.T_cur-self.T_up) / (self.T_i - self.T_up))) / 2
                    for base_lr in self.base_lrs]

    def step(self, epoch=None):
        if epoch is None:
            epoch = self.last_epoch + 1
            self.T_cur = self.T_cur + 1
            if self.T_cur >= self.T_i:
                self.cycle += 1
                self.T_cur = self.T_cur - self.T_i
                self.T_i = (self.T_i - self.T_up) * self.T_mult + self.T_up
        else:
            if epoch >= self.T_0:
                if self.T_mult == 1:
                    self.T_cur = epoch % self.T_0
                    self.cycle = epoch // self.T_0
                else:
                    n = int(math.log((epoch / self.T_0 * (self.T_mult - 1) + 1), self.T_mult))
                    self.cycle = n
                    self.T_cur = epoch - self.T_0 * (self.T_mult ** n - 1) / (self.T_mult - 1)
                    self.T_i = self.T_0 * self.T_mult ** (n)
            else:
                self.T_i = self.T_0
                self.T_cur = epoch
                
        self.eta_max = self.base_eta_max * (self.gamma**self.cycle)
        self.last_epoch = math.floor(epoch)
        for param_group, lr in zip(self.optimizer.param_groups, self.get_lr()):
            param_group['lr'] = lr
from torch.optim.optimizer import Optimizer
class ReduceLROnPlateau(object):
    """Reduce learning rate when a metric has stopped improving.
    Models often benefit from reducing the learning rate by a factor
    of 2-10 once learning stagnates. This scheduler reads a metrics
    quantity and if no improvement is seen for a 'patience' number
    of epochs, the learning rate is reduced.
    Args:
        optimizer (Optimizer): Wrapped optimizer.
        mode (str): One of `min`, `max`. In `min` mode, lr will
            be reduced when the quantity monitored has stopped
            decreasing; in `max` mode it will be reduced when the
            quantity monitored has stopped increasing. Default: 'min'.
        factor (float): Factor by which the learning rate will be
            reduced. new_lr = lr * factor. Default: 0.1.
        patience (int): Number of epochs with no improvement after
            which learning rate will be reduced. Default: 10.
        verbose (bool): If True, prints a message to stdout for
            each update. Default: False.
        threshold (float): Threshold for measuring the new optimum,
            to only focus on significant changes. Default: 1e-4.
        threshold_mode (str): One of `rel`, `abs`. In `rel` mode,
            dynamic_threshold = best * ( 1 + threshold ) in 'max'
            mode or best * ( 1 - threshold ) in `min` mode.
            In `abs` mode, dynamic_threshold = best + threshold in
            `max` mode or best - threshold in `min` mode. Default: 'rel'.
        cooldown (int): Number of epochs to wait before resuming
            normal operation after lr has been reduced. Default: 0.
        min_lr (float or list): A scalar or a list of scalars. A
            lower bound on the learning rate of all param groups
            or each group respectively. Default: 0.
        eps (float): Minimal decay applied to lr. If the difference
            between new and old lr is smaller than eps, the update is
            ignored. Default: 1e-8.
    Example:
        >>> optimizer = torch.optim.SGD(model.parameters(), lr=0.1, momentum=0.9)
        >>> scheduler = ReduceLROnPlateau(optimizer, 'min')
        >>> for epoch in range(10):
        >>>     train(...)
        >>>     val_loss = validate(...)
        >>>     # Note that step should be called after validate()
        >>>     scheduler.step(val_loss)
    """

    def __init__(self, optimizer, mode='min', factor=0.1, patience=10,
                 verbose=False, threshold=1e-4, threshold_mode='rel',
                 cooldown=0, min_lr=0, eps=1e-8):

        if factor >= 1.0:
            raise ValueError('Factor should be < 1.0.')
        self.factor = factor

        if not isinstance(optimizer, Optimizer):
            raise TypeError('{} is not an Optimizer'.format(
                type(optimizer).__name__))
        self.optimizer = optimizer

        if isinstance(min_lr, list) or isinstance(min_lr, tuple):
            if len(min_lr) != len(optimizer.param_groups):
                raise ValueError("expected {} min_lrs, got {}".format(
                    len(optimizer.param_groups), len(min_lr)))
            self.min_lrs = list(min_lr)
        else:
            self.min_lrs = [min_lr] * len(optimizer.param_groups)

        self.patience = patience
        self.verbose = verbose
        self.cooldown = cooldown
        self.cooldown_counter = 0
        self.mode = mode
        self.threshold = threshold
        self.threshold_mode = threshold_mode
        self.best = None
        self.num_bad_epochs = None
        self.mode_worse = None  # the worse value for the chosen mode
        self.is_better = None
        self.eps = eps
        self.last_epoch = -1
        self._init_is_better(mode=mode, threshold=threshold,
                             threshold_mode=threshold_mode)
        self._reset()

    def _reset(self):
        """Resets num_bad_epochs counter and cooldown counter."""
        self.best = self.mode_worse
        self.cooldown_counter = 0
        self.num_bad_epochs = 0

    """ Status codes to be returned by ReduceLROnPlateau.step() """
    STATUS_WAITING = 1
    STATUS_UPDATED_BEST = 2
    STATUS_REDUCED_LR = 3

    def step(self, metrics, epoch=None):
        current = metrics
        if epoch is None:
            epoch = self.last_epoch = self.last_epoch + 1
        self.last_epoch = epoch

        if self.in_cooldown:
            self.cooldown_counter -= 1
            self.num_bad_epochs = 0  # ignore any bad epochs in cooldown
        else:
            self.num_bad_epochs += 1

        if self.is_better(current, self.best):
            self.best = current
            self.num_bad_epochs = 0
            return self.STATUS_UPDATED_BEST
        else:
            if self.num_bad_epochs > self.patience:
                self._reduce_lr(epoch)
                self.cooldown_counter = self.cooldown
                self.num_bad_epochs = 0
                return self.STATUS_REDUCED_LR
            else:
                return self.STATUS_WAITING

    def _reduce_lr(self, epoch):
        for i, param_group in enumerate(self.optimizer.param_groups):
            old_lr = float(param_group['lr'])
            new_lr = max(old_lr * self.factor, self.min_lrs[i])
            if old_lr - new_lr > self.eps:
                param_group['lr'] = new_lr
                if self.verbose:
                    print('Epoch {:5d}: reducing learning rate'
                          ' of group {} to {:.4e}.'.format(epoch, i, new_lr))

    @property
    def in_cooldown(self):
        return self.cooldown_counter > 0

    def _init_is_better(self, mode, threshold, threshold_mode):
        if mode not in {'min', 'max'}:
            raise ValueError('mode ' + mode + ' is unknown!')
        if threshold_mode not in {'rel', 'abs'}:
            raise ValueError('threshold mode ' + mode + ' is unknown!')
        if mode == 'min' and threshold_mode == 'rel':
            rel_epsilon = 1. - threshold
            self.is_better = lambda a, best: a < best * rel_epsilon
            self.mode_worse = float('Inf')
        elif mode == 'min' and threshold_mode == 'abs':
            self.is_better = lambda a, best: a < best - threshold
            self.mode_worse = float('Inf')
        elif mode == 'max' and threshold_mode == 'rel':
            rel_epsilon = threshold + 1.
            self.is_better = lambda a, best: a > best * rel_epsilon
            self.mode_worse = -float('Inf')
        else:  # mode == 'max' and epsilon_mode == 'abs':
            self.is_better = lambda a, best: a > best + threshold
            self.mode_worse = -float('Inf')

            
class ReduceLROnPlateauWithBacktrack(ReduceLROnPlateau):
    """Load training state from the best epoch and reduce learning
    rate when a metric has stopped improving.
    Models often benefit from reducing the learning rate by a factor
    of 2-10 once learning stagnates. This scheduler reads a metrics
    quantity and if no improvement is seen for a 'patience' number
    of epochs, the learning rate is reduced, and the best state is
    loaded.
    Args:
        optimizer (Optimizer): Wrapped optimizer.
        model (torch.nn.Module): Model to be saved and backtracked.
        filename (str): Directory to save the best state.
        mode (str): One of `min`, `max`. In `min` mode, lr will
            be reduced when the quantity monitored has stopped
            decreasing; in `max` mode it will be reduced when the
            quantity monitored has stopped increasing. Default: 'min'.
        factor (float): Factor by which the learning rate will be
            reduced. new_lr = lr * factor. Default: 0.1.
        patience (int): Number of epochs with no improvement after
            which learning rate will be reduced. Default: 10.
        verbose (bool): If True, prints a message to stdout for
            each update. Default: False.
        threshold (float): Threshold for measuring the new optimum,
            to only focus on significant changes. Default: 1e-4.
        threshold_mode (str): One of `rel`, `abs`. In `rel` mode,
            dynamic_threshold = best * ( 1 + threshold ) in 'max'
            mode or best * ( 1 - threshold ) in `min` mode.
            In `abs` mode, dynamic_threshold = best + threshold in
            `max` mode or best - threshold in `min` mode. Default: 'rel'.
        cooldown (int): Number of epochs to wait before resuming
            normal operation after lr has been reduced. Default: 0.
        min_lr (float or list): A scalar or a list of scalars. A
            lower bound on the learning rate of all param groups
            or each group respectively. Default: 0.
        eps (float): Minimal decay applied to lr. If the difference
            between new and old lr is smaller than eps, the update is
            ignored. Default: 1e-8.
    Example:
        >>> optimizer = torch.optim.SGD(model.parameters(), lr=0.1, momentum=0.9)
        >>> scheduler = ReduceLROnPlateauWithBacktrack(optimizer, model)
        >>> for epoch in range(10):
        >>>     train(...)
        >>>     val_loss = validate(...)
        >>>     # Note that step should be called after validate()
        >>>     scheduler.step(val_loss)
    """

    def __init__(self, optimizer, model, filename, warmup_steps, **kwargs):
        self.filename = filename
        self.model = model
        self.warmup_steps = warmup_steps + 1
        self.warmup_step_curr = 1
        self.warmup_phase = False
        self.init_lrs_for_warmup = []
        
         
        
        super(ReduceLROnPlateauWithBacktrack, self).__init__(
            optimizer=optimizer, **kwargs)
        for group in self.optimizer.param_groups:
            self.init_lrs_for_warmup.append(group['lr'])

    def step(self, metrics, epoch=None):
        if self.warmup_step_curr < self.warmup_steps + 1:
            self.warmup_phase = True
            factor = float(self.warmup_step_curr) / float(max(1, self.warmup_steps))
            print('warmup with factor ', factor)
            for init_lr, group in zip(self.init_lrs_for_warmup, self.optimizer.param_groups):
                group['lr'] = init_lr * (0.0001)**(1 - factor)
            self.warmup_step_curr += 1
            return 
        self.warmup_phase = False
        print('stepping on metric: %f' % metrics)      
        status = super(ReduceLROnPlateauWithBacktrack, self).step(metrics=metrics, epoch=epoch)
        if status == self.STATUS_UPDATED_BEST:
            torch.save(
                {'model': self.model.state_dict(), 'optim': self.optimizer.state_dict()},
                self.filename)
        elif status == self.STATUS_REDUCED_LR:
            new_lrs = [group['lr'] for group in self.optimizer.param_groups]
            backtrack_dict = torch.load(self.filename)
            self.optimizer.load_state_dict(backtrack_dict['optim'])
            self.model.load_state_dict(backtrack_dict['model'])
            # Note that new_lr might not be saved_lr * gamma
            for new_lr, group in zip(new_lrs, self.optimizer.param_groups):
                group['lr'] = new_lr
                
                
                
# class AugScheduler:
#     def __init__(self, start_lr, patience, factor=0.5, mode='max', threshold=1e-4):
#         self.curr_lr = start_lr
#         self.same_lr_epoch_counter = 0
#         self.mode = mode
#         self.threshold = threshold
#         self.patience = patience
#         self.factor = factor
#         self.best_score_epochs = 0
#         self.p_multiplier = 1
#         self.verbose = True
#         if mode == 'max':
#             self.best_score = 0
    
#     def _reset(self, lr):
#         self.same_lr_epoch_counter = 0
#         self.p_multiplier = 1
#         self.curr_lr = lr
#         if self.mode == 'max':
#             self.best_score = 0
#         self.best_score_epochs = 0
        
#     def _print_status(self):
#         print('same_lr_epoch_counter', self.same_lr_epoch_counter)
#         print('curr_lr', self.curr_lr)
#         print('best_score', self.best_score)
#         print('p_multiplier', self.p_multiplier)
#         print('p_best_score_epochs', self.best_score_epochs)
    
#     def get_p(self):
#         return self.p_multiplier
    
#     def step_and_get_p(self, metric, lr):
        
            
#         if self.curr_lr != lr:
#             if self.verbose:
#                 print('stepping the lr, resetting all the metrics')
#             self._reset(lr)
#         else:
#             self.same_lr_epoch_counter += 1
            
#         if self.mode == 'max':
#             if (metric - self.best_score) > self.threshold:
#                 if self.verbose:
#                     print('best metric achieved')
#                 self.best_score = metric
#                 self.best_score_epochs = 0
#             else:
#                 self.best_score_epochs += 1
        
#         if self.best_score_epochs > self.patience:
#             self.best_score = metric
#             self.best_score_epochs = 0
#             self.p_multiplier = self.p_multiplier * self.factor
#         if self.verbose:
#             self._print_status()
#         return  self.p_multiplier 

        
class AugScheduler:
    def __init__(self, start_lr, patience, factor=0.5, mode='max', threshold=1e-4, patience_add=0):
        self.curr_lr = start_lr
        self.same_lr_epoch_counter = 0
        self.mode = mode
        self.threshold = threshold
        self.patience = patience
        self.curr_patience = self.patience
        self.factor = factor
        self.best_score_epochs = 0
        self.p_multiplier = 1
        self.verbose = True
        self.patience_add = patience_add
        if mode == 'max':
            self.best_score = 0
    
    def _reset(self, lr):
        self.same_lr_epoch_counter = 0
        self.p_multiplier = 1
        self.curr_lr = lr
        if self.mode == 'max':
            self.best_score = 0
        self.best_score_epochs = 0
        self.curr_patience = self.patience
        
    def _print_status(self):
        print('same_lr_epoch_counter', self.same_lr_epoch_counter)
        print('curr_lr', self.curr_lr)
        print('best_score', self.best_score)
        print('p_multiplier', self.p_multiplier)
        print('p_best_score_epochs', self.best_score_epochs)
        print('curr_patience', self.curr_patience)
    
    def get_p(self):
        return self.p_multiplier
    
    def step_and_get_p(self, metric, lr):
        
            
        if self.curr_lr != lr:
            if self.verbose:
                print('stepping the lr, resetting all the metrics')
            self._reset(lr)
        else:
            self.same_lr_epoch_counter += 1
            
        if self.mode == 'max':
            if (metric - self.best_score) > self.threshold:
                if self.verbose:
                    print('best metric achieved')
                self.best_score = metric
                self.best_score_epochs = 0
            else:
                self.best_score_epochs += 1
        
        if self.best_score_epochs > self.curr_patience:
            self.best_score = metric
            self.best_score_epochs = 0
            self.p_multiplier = self.p_multiplier * self.factor
            self.curr_patience = self.curr_patience + self.patience_add
        if self.verbose:
            self._print_status()
        return  self.p_multiplier 
    
        ###############
        
        
class AugSchedulerAdder:
    def __init__(self, start_lr, patience, factor=0.5, mode='max', threshold=1e-4, patience_add=0):
        self.curr_lr = start_lr
        self.same_lr_epoch_counter = 0
        self.mode = mode
        self.threshold = threshold
        self.patience = patience
        self.curr_patience = self.patience
        self.factor = factor
        self.best_score_epochs = 0
        self.p_adder = 0
        self.verbose = True
        self.patience_add = patience_add
        if mode == 'max':
            self.best_score = 0
    
    def _reset(self, lr):
        self.same_lr_epoch_counter = 0
        self.p_adder = 0
        self.curr_lr = lr
        if self.mode == 'max':
            self.best_score = 0
        self.best_score_epochs = 0
        self.curr_patience = self.patience
        
    def _print_status(self):
        print('same_lr_epoch_counter', self.same_lr_epoch_counter)
        print('curr_lr', self.curr_lr)
        print('best_score', self.best_score)
        print('p_adder', self.p_adder)
        print('p_best_score_epochs', self.best_score_epochs)
        print('curr_patience', self.curr_patience)
    
    def get_p(self):
        return self.p_adder
    
    def step_and_get_p(self, metric, lr):
        
            
        if self.curr_lr != lr:
            if self.verbose:
                print('stepping the lr, resetting all the metrics')
            self._reset(lr)
        else:
            self.same_lr_epoch_counter += 1
            
        if self.mode == 'max':
            if (metric - self.best_score) > self.threshold:
                if self.verbose:
                    print('best metric achieved')
                self.best_score = metric
                self.best_score_epochs = 0
            else:
                self.best_score_epochs += 1
        
        if self.best_score_epochs > self.curr_patience:
            self.best_score = metric
            self.best_score_epochs = 0
            self.p_adder = self.p_adder + self.factor
            self.curr_patience = self.curr_patience + self.patience_add
        if self.verbose:
            self._print_status()
        return  self.p_adder 
    
        ###############
        
        