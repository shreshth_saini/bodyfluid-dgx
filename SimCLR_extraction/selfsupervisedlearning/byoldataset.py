import os
import torch
import torchvision.datasets as datasets
import torch.utils.data as data
from PIL import Image
from torchvision import transforms 
from glob import glob
from torch.utils.data import DataLoader, Dataset
import glob
import numpy as np
from skimage import io
from torch.utils.data import DataLoader, Dataset

batch_size = 512 # v3
# batch_size = 128 # v4
# batch_size = 128 # v5
num_workers = 16
img_size = 150

class BYOLImageDataset(Dataset):
    def __init__(self, file_dirs, tfms=None, return_fp=False):
#         file_paths = [fp for file_dir in file_dirs for fp in glob.glob(file_dir, recursive=True)]
        
        file_paths = []
        
        for file_dir in file_dirs:
            if file_dir.find('*') != -1:
                file_paths.extend(glob.glob(file_dir, recursive=True))
            else:
                file_paths.append(file_dir)
        
        print('Number of Images: ', len(file_paths))
        file_paths = [x for x in file_paths if (x.lower().endswith("bmp") or x.lower().endswith('png'))]

        self.filepaths = file_paths 
        self.tfms = tfms
        self.return_fp = return_fp
        print('transformations', tfms)
    def __len__(self):
        return len(self.filepaths)

    def __getitem__(self, idx):
        with open(self.filepaths[idx], 'rb') as f:
            X = Image.open(f).convert('RGB')
        if self.return_fp:
            return self.tfms(X), self.filepaths[idx]
        return self.tfms(X)

train_tfms =  transforms.Compose([
        transforms.RandomResizedCrop(size=img_size, scale=(0.7, 1.0)), # this is augmentation 
        transforms.ToTensor(), 
        transforms.Normalize(mean=[0.4914, 0.4822, 0.4465], std=[0.2023, 0.1994, 0.2010])
])

test_tfms =  transforms.Compose([
        transforms.Resize((img_size, img_size)),
        transforms.ToTensor(), 
        transforms.Normalize(mean=[0.4914, 0.4822, 0.4465], std=[0.2023, 0.1994, 0.2010])
])

aug_transforms = transforms.Compose([
            
            transforms.RandomHorizontalFlip(),
            # aadded 
            transforms.RandomVerticalFlip(),    
            transforms.RandomRotation(90,),
            transforms.RandomApply([
                transforms.ColorJitter(brightness=0.4, contrast=0.4, saturation=0.4, hue=0.1)
            ], p=0.8),
            transforms.RandomGrayscale(p=0.2),
        ])
    
    
def get_train_dl(dirs, batch_size=batch_size, num_workers=num_workers):
    print('batch size', batch_size)
    ds = BYOLImageDataset(file_dirs=dirs, tfms=train_tfms, return_fp=False)
    dl = DataLoader(ds,batch_size=batch_size, shuffle=True, num_workers=num_workers)
    return dl

def get_test_dl(dirs, batch_size=batch_size, num_workers=num_workers):
    print('batch size', batch_size)
    ds = BYOLImageDataset(file_dirs=dirs, tfms=test_tfms, return_fp=True)
    dl = DataLoader(ds, batch_size=batch_size, shuffle=False, num_workers=num_workers)

    return dl





