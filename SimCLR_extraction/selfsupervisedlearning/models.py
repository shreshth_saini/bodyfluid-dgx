import torchvision.models as models
import torch.nn as nn
import torch
import torch.nn.functional as F
from efficientnet_pytorch import EfficientNet

class Model(torch.nn.Module):
    def __init__(self, features_dim=128):
        super(Model, self).__init__()
        self.backbone = models.resnet18(pretrained=True)
        self.backbone.fc = torch. nn.Linear(512, 256)
        self.backbone_dim = self.backbone.fc.out_features
        self.head = nn.Sequential(
                    nn.Linear(self.backbone_dim, self.backbone_dim),
                    nn.BatchNorm1d(self.backbone_dim),
                    nn.ReLU(), nn.Linear(self.backbone_dim, features_dim))
 
    def forward(self, x):
        features = self.head(self.backbone(x))
        features = F.normalize(features, dim = -1)
        return features

class ResNetModel(torch.nn.Module):
    def __init__(self, features_dim=128, v='18', mlp_dim=512):
        super(ResNetModel, self).__init__()
        self.backbone = getattr(models, f'resnet{v}')(pretrained=True)
        backbone_dim = self.backbone.fc.in_features
#         self.backbone = nn.Sequential(*list(model.children())[:-1])
        self.backbone.fc = nn.Identity()
        self.head = nn.Sequential(
                    nn.Flatten(),
                    nn.Linear(backbone_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(), nn.Linear(mlp_dim, features_dim))
 
    def forward(self, x):
        x = self.backbone(x)
        features = self.head(x)
        features = F.normalize(features, dim = -1)
        return features

    

class EfficientNetModel(torch.nn.Module):
    def __init__(self, features_dim=128, v='b0', mlp_dim=1024):
        super(EfficientNetModel, self).__init__()
        self.backbone = EfficientNet.from_pretrained(f'efficientnet-{v}')
        backbone_dim = self.backbone._fc.in_features
#         self.backbone = nn.Sequential(*list(model.children())[:-3]) # till avgpool
        
        self.backbone._dropout = nn.Identity()
        self.backbone._fc = nn.Identity()
        self.backbone._swish = nn.Identity()
        self.head = nn.Sequential(
                    nn.Flatten(),
                    nn.Linear(backbone_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, features_dim),
        )
 
    def forward(self, x):
        x = self.backbone(x)
        features = self.head(x)
        features = F.normalize(features, dim = -1)
        return features

    
class EfficientNetModelMLP(torch.nn.Module):
    def __init__(self, features_dim=2048, v='b2', mlp_dim=2048):
        super(EfficientNetModelMLP, self).__init__()
        self.backbone = EfficientNet.from_pretrained(f'efficientnet-{v}')
        backbone_dim = self.backbone._fc.in_features
#         self.backbone = torch.nn.Sequential(*(list(self.backbone.children())[:-3]))
#         self.backbone = nn.Sequential(*list(model.children())[:-3]) # till avgpool
#         backbone_dim = self.backbone[-2].num_features
        self.backbone._dropout = nn.Identity()
        self.backbone._fc = nn.Identity()
        self.backbone._swish = nn.Identity()
        self.mlp = nn.Sequential(
                    nn.Flatten(),
                    nn.Linear(backbone_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
        )
        self.head = nn.Sequential(
                    
                    nn.Linear(mlp_dim, mlp_dim//4),
                    nn.BatchNorm1d(mlp_dim//4),
                    nn.ReLU(),
                    nn.Linear(mlp_dim//4, features_dim),
        )
    def backbone_mlp(self, x):
        x = self.backbone(x)
        x = self.mlp(x)
        return x
    
    def forward(self, x):
        x = self.backbone(x)
        x = self.mlp(x)
        x = self.head(x)
        x = F.normalize(x, dim = -1)
        return x

   
class DenseNetModel(torch.nn.Module):
    def __init__(self, features_dim=128):
        super(DesNet, self).__init__()
        self.backbone = models.densenet161(pretrained=True)
#         self.backbone.fc = torch. nn.Linear(512, 256)
        self.backbone_dim = self.backbone.classifier.out_features
        self.head = nn.Sequential(
                    nn.Linear(self.backbone_dim, self.backbone_dim),
                    nn.BatchNorm1d(self.backbone_dim),
                    nn.ReLU(), nn.Linear(self.backbone_dim, features_dim))
 
    def forward(self, x):
        features = self.head(self.backbone(x))
        features = F.normalize(features, dim = -1)
        return features
    

class ResNet50Model(torch.nn.Module):
    def __init__(self, features_dim=256):
        super(ResNet50Model, self).__init__()
        self.backbone = models.resnet50(pretrained=True)
#         self.backbone.fc = torch. nn.Linear(1000, 1000)
        self.backbone_dim = self.backbone.fc.out_features
        self.head = nn.Sequential(
                    nn.Linear(self.backbone_dim, self.backbone_dim),
                    nn.BatchNorm1d(self.backbone_dim),
                    nn.ReLU(), nn.Linear(self.backbone_dim, features_dim))
 
    def forward(self, x):
        features = self.head(self.backbone(x))
        features = F.normalize(features, dim = -1)
        return features

    
class ResnetMLP(torch.nn.Module):
    def __init__(self, features_dim=1024, v='18', mlp_dim=1024):
        
        super(ResnetMLP, self).__init__()
        self.backbone = getattr(models, f'resnet{v}')(pretrained=True)
        backbone_dim = self.backbone.fc.in_features
        self.backbone.fc = nn.Identity()
    
        self.mlp = nn.Sequential(
                    nn.Flatten(),
                    nn.Linear(backbone_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
        )
        self.head = nn.Sequential(
                    
                    nn.Linear(mlp_dim, mlp_dim//4),
                    nn.BatchNorm1d(mlp_dim//4),
                    nn.ReLU(),
                    nn.Linear(mlp_dim//4, features_dim),
        )
    def backbone_mlp(self, x):
        x = self.backbone(x)
        x = self.mlp(x)
        return x
    
    def forward(self, x):
        x = self.backbone(x)
        x = self.mlp(x)
        x = self.head(x)
        x = F.normalize(x, dim = -1)
        return x
    
class MobilenetMLP(torch.nn.Module):
    def __init__(self, features_dim=1024, v='v2', mlp_dim=1024):
        
        super(MobilenetMLP, self).__init__()
        self.backbone = torch.hub.load('pytorch/vision:v0.6.0', f'mobilenet_{v}', pretrained=True)
        backbone_dim = self.backbone.classifier[1].in_features
        self.backbone = nn.Sequential(self.backbone.features, nn.AdaptiveAvgPool2d(output_size=(1, 1)))
    
        self.mlp = nn.Sequential(
                    nn.Flatten(),
                    nn.Linear(backbone_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
                    nn.ReLU(),
                    nn.Linear(mlp_dim, mlp_dim),
                    nn.BatchNorm1d(mlp_dim),
        )
        self.head = nn.Sequential(
                    
                    nn.Linear(mlp_dim, mlp_dim//4),
                    nn.BatchNorm1d(mlp_dim//4),
                    nn.ReLU(),
                    nn.Linear(mlp_dim//4, features_dim),
        )
    def backbone_mlp(self, x):
        x = self.backbone(x)
        x = self.mlp(x)
        return x
    
    def forward(self, x):
        x = self.backbone(x)
        x = self.mlp(x)
        x = self.head(x)
        x = F.normalize(x, dim = -1)
        return x
try:
    from vit_pytorch import ViT

    class ViTMLP(torch.nn.Module):
        def __init__(self, features_dim=1024, v=None, mlp_dim=1024, img_size=150, patch_size=25):

            super(ViTMLP, self).__init__()
            self.backbone = model = ViT(
                image_size = img_size,
                patch_size = patch_size,
                num_classes = 1000,
                dim = features_dim,
                depth = 6,
                heads = 8,
                mlp_dim = features_dim
            )
            backbone_dim = features_dim
            self.backbone.mlp_head = torch.nn.Identity()
            self.mlp = nn.Sequential(
                        nn.Flatten(),
                        nn.Linear(backbone_dim, mlp_dim),
                        nn.BatchNorm1d(mlp_dim),
                        nn.ReLU(),
                        nn.Linear(mlp_dim, mlp_dim),
                        nn.BatchNorm1d(mlp_dim),
                        nn.ReLU(),
                        nn.Linear(mlp_dim, mlp_dim),
                        nn.BatchNorm1d(mlp_dim),
            )
            self.head = nn.Sequential(

                        nn.Linear(mlp_dim, mlp_dim//4),
                        nn.BatchNorm1d(mlp_dim//4),
                        nn.ReLU(),
                        nn.Linear(mlp_dim//4, features_dim),
            )
        def backbone_mlp(self, x):
            x = self.backbone(x)
            x = self.mlp(x)
            return x

        def forward(self, x):
            x = self.backbone(x)
            x = self.mlp(x)
            x = self.head(x)
            x = F.normalize(x, dim = -1)
            return x
except Exception as e:
    print("couldn't import Visual Transformers")