target = 'dst'

import os, glob

img_list = [i for i in glob.glob(target+os.sep+'**', recursive=True) if i.endswith('.png')]
img_dict = {i.split('$')[-1]:i for i in img_list}

dup_dict = {}
for k, v in img_dict.items():
	if k in dup_dict:
		dup_dict[k].append(v)
	else:
		dup_dict[k] = [v]

for k, v in dup_dict.items():
	if len(v) > 1:
		print(v)