target = 'clusters_bodyfluid'

import os, glob
dir_list = [i for i in glob.glob(target+os.sep+'**') if os.path.isdir(i)]

with open('body_fluid_0.csv', mode='w') as f:
	for d in dir_list:
		dir_name = d.split(os.sep)[-1]
		print(dir_name)
		f.write(dir_name+',\n')
