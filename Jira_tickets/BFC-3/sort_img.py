target = 'b.csv'
img_dir = 'imgs'

import pandas as pd
import os, glob, shutil
from tqdm import tqdm

df = pd.read_csv(target, index_col=0)
dir2label_dict = {}
for idx, rows in df.iterrows():
	label = rows[0]
	if label != 'x':
		dir2label_dict[str(idx)] = label

img_list = [i for i in glob.glob(img_dir+os.sep+'**', recursive=True) if i.endswith('.png')]
img_dict = {i.split(os.sep)[-1]:i for i in img_list}

print('all images in {0} are {1}'.format(img_dir, len(img_list)))
file2label_dict = {}
for img_path in img_list:
	img_body_list = img_path.split(os.sep)
	file_name = img_body_list[-1]
	label = img_body_list[-2]
	if label in dir2label_dict:
		#print(label, dir2label_dict[label])
		label = dir2label_dict[label]
	file2label_dict[file_name] = str(label)
	#print(file_name, label)

for k, v in tqdm(file2label_dict.items()):
	dst_dir = 'dst'+os.sep+v
	if not os.path.exists(dst_dir):
		os.makedirs(dst_dir)
	shutil.copy(img_dict[k], dst_dir+os.sep+k)
