target = 'dst'

import os, glob, pickle

img_list = [i for i in glob.glob(target+os.sep+'**', recursive=True) if i.endswith('.png')]
img_dict = {}
for i in img_list:
	img_body_list = i.split(os.sep)
	file_name = img_body_list[-1]
	label= img_body_list[-2]
	img_dict[file_name] = label

print(len(img_dict))

with open('bfc-3.pic', mode='wb') as f:
	pickle.dump(img_dict, f)
